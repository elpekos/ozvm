Git repository for Cambridge Z88 Emulator
GPL V2 software

    ,o888888o.   8888888888',8888' `888b           ,8' ,8.       ,8.
 . 8888     `88.        ,8',8888' 8.`888b         ,8' ,888.     ,888.
,8 8888       `8b      ,8',8888'  `8.`888b       ,8' .`8888.   .`8888.
88 8888        `8b    ,8',8888'    `8.`888b     ,8' ,8.`8888. ,8.`8888.
88 8888         88   ,8',8888'      `8.`888b   ,8' ,8'8.`8888,8^8.`8888.
88 8888         88  ,8',8888'        `8.`888b ,8' ,8' `8.`8888' `8.`8888.
88 8888        ,8P ,8',8888'          `8.`888b8' ,8'   `8.`88'   `8.`8888.
`8 8888       ,8P ,8',8888'            `8.`888' ,8'     `8.`'     `8.`8888.
 ` 8888     ,88' ,8',8888'              `8.`8' ,8'       `8        `8.`8888.
    `8888888P'  ,8',8888888888888        `8.` ,8'         `         `8.`8888.



============================================================================
Introduction to OZvm - the Cambridge Z88 emulator
============================================================================

Welcome to the OZvm development Git repository - the place to get the
bleeding edge work of the Cambridge Z88 emulator.

Why the "OZvm" name?

It is a combination of "OZ" - the name of the Cambridge Z88 operating system
and "vm" - the shortcut for virtual machine.

OZvm is principally a hardware emulation of the Cambridge Z88 portable
computer - a Z80 CPU system with 4Mb addressable memory, a monochrome 640x64
LCD and RS-232 serial port. It allows to install any ROM or application card
binary that also runs on a real Cambridge Z88.

OZvm is part of the Z88 Workbench tool-chain used to build software for the
Cambridge Z88. It has been implemented using Java SE V1.7 or later. You only
need to have latest Java Runtime installed on your desktop system. OZvm is
100% compatible with OpenJDK.

Further, you can easily copy or move the complete directory structure to
another media and carry on working, including getting new updates from the
Git repository or committing changes back to the repository (if you have been
granted write access).


============================================================================
The OZvm Project resources
============================================================================
https://gitlab.com/z88/ozvm                        (Project issues)
https://gitlab.com/z88/ozvm/-/wikis/home           (Project wiki)



============================================================================
Using the Desktop Installer for 32bit Linux and Windows
============================================================================
OZvm is available as a Desktop Installer available for Linux and Windows,
requiring no Java Runtime environment pre-installed. OZvm has been compiled
as a native application for Intel 32bit Linux and Windows OS using Excelsior
JET V7.6 (http://www.excelsior-usa.com).

The installer will provide icons on your desktop for running the Cambridge
Z88 emulator.

Only V1.1 or later are available as compiled binaries - earlier releases
are provided only as executable JAR files (JRE requirement).



============================================================================
Compiling OZvm
============================================================================
You can always check out the bleeding edge repository and compile the latest
version into a Java executable, a JAR file.

To compile the Java application you need the Java Runtime V1.7 Environment
installed on your desktop operating system.

To get OZvm compiled into an executable program on your operating system,
execute the makejar script:

    makejar.bat (or ./makejar.sh)

This will create the z88.jar executable Java file, z88.jar

Run OZvm with

    java -jar z88.jar

Windows users may create an EXE program (a Java JAR file wrapper). You need
to install Launch4J (http://launch4j.sourceforge.net/), and edit your PATH
environment variable to include the <launch4j install directory>. The
following script both compiles the Jar file and makes a z88.exe program:

    makeexe.bat
