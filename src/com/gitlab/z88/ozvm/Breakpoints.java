/*
 * Breakpoints.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:hello@bits4fun.net">Gunther Strube</A>
 * (C) Gunther Strube (hello@bits4fun.net) 2000-2021
 *
 */
package com.gitlab.z88.ozvm;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Manage breakpoint addresses in Z88 virtual machine.
 */
public class Breakpoints {

    private Map breakPoints;
    private Breakpoint bpSearchKey;
    private boolean breakAtOZCall;
    private boolean displayOZCall;

    /**
     * Just instantiate this Breakpoint Manager
     */
    public Breakpoints() {
        breakAtOZCall = false;
        displayOZCall = false;
        breakPoints = new HashMap();
        bpSearchKey = new Breakpoint(0);    // just create a dummy search key object (used by internal lookup)
    }


    /**
     * Enable (or disable) break at OZ Call (RST 20H instruction).
     */
    public void toggleBreakOzCall() {
        breakAtOZCall = !breakAtOZCall;
    }

    /**
     * Enable (or disable) display of OZ Call (RST 20H instruction) in Runtime Messages.
     */
    public void toggleDisplayOzCall() {
        displayOZCall = !displayOZCall;
    }

    /**
     * Return <true> if break is enabled for OZ Calls
     */
    public boolean isBreakOzCallActive() {
        return breakAtOZCall;
    }

    /**
     * Return <true> if display is enabled for OZ Calls
     */
    public boolean isDisplayOzCallActive() {
        return displayOZCall;
    }

    /**
     * Remove breakpoint, if created
     *
     * @param address 24bit extended address
     */
    public void clearBreakpoint(int bpAddress) {
        Breakpoint bp = new Breakpoint(bpAddress);
        if (breakPoints.containsKey(bp) == true) {
            breakPoints.remove(bp);
            Z88.getInstance().getMemory().clearBreakpoint(bpAddress);
        }
    }

    /**
     * Add (if not created) breakpoint
     *
     * @param address 24bit extended address
     */
    public void setBreakpoint(int bpAddress) {
        Breakpoint bp = new Breakpoint(bpAddress);
        if (breakPoints.containsKey(bp) == false) {
            breakPoints.put(bp, bp);
            Z88.getInstance().getMemory().setBreakpoint(bpAddress);
        }
    }

    /**
     * Add (if not created) a single-stop breakpoint
     *
     * @param address 24bit extended address
     */
    public void setSingleStopBreakpoint(int bpAddress) {
        Breakpoint bp = new Breakpoint(bpAddress, true, true);
        if (breakPoints.containsKey(bp) == false) {
            breakPoints.put(bp, bp);
            Z88.getInstance().getMemory().setBreakpoint(bpAddress);
        }
    }

    /**
     * Add (if not created) or remove breakpoint (if previously created).
     *
     * @param address 24bit extended address
     */
    public void toggleBreakpoint(int bpAddress) {
        Breakpoint bp = new Breakpoint(bpAddress);
        if (breakPoints.containsKey(bp) == false) {
            breakPoints.put(bp, bp);
            Z88.getInstance().getMemory().setBreakpoint(bpAddress);
        } else {
            breakPoints.remove(bp);
            Z88.getInstance().getMemory().clearBreakpoint(bpAddress);
        }
    }

    /**
     * Add (if not created) or remove breakpoint (if prev. created).
     *
     * @param address 24bit extended address
     * @param stopStatus
     */
    public void toggleBreakpoint(int bpAddress, boolean stopStatus) {
        Breakpoint bp = new Breakpoint(bpAddress, stopStatus);
        if (breakPoints.containsKey(bp) == false) {
            breakPoints.put(bp, bp);
            Z88.getInstance().getMemory().setBreakpoint(bpAddress);
        } else {
            breakPoints.remove(bp);
            Z88.getInstance().getMemory().clearBreakpoint(bpAddress);
        }
    }

    /**
     * Add (if not created) or remove breakpoint (if prev. created).
     *
     * @param address 24bit extended address
     * @param stopStatus
     */
    public void toggleBreakpoint(int bpAddress, ArrayList<String> brkpCmds) {
        Breakpoint bp = new Breakpoint(bpAddress, brkpCmds);
        if (breakPoints.containsKey(bp) == false) {
            breakPoints.put(bp, bp);
            Z88.getInstance().getMemory().setBreakpoint(bpAddress);
        } else {
            breakPoints.remove(bp);
            Z88.getInstance().getMemory().clearBreakpoint(bpAddress);
        }
    }

    public boolean hasCommands(int bpAddress) {
        bpSearchKey.setBpAddress(bpAddress);
        Breakpoint bpv = (Breakpoint) breakPoints.get(bpSearchKey);
        if (bpv != null) {
            return bpv.hasCommands();
        } else {
            return false;
        }
    }

    public void runCommands(int bpAddress) {
        bpSearchKey.setBpAddress(bpAddress);
        Breakpoint bpv = (Breakpoint) breakPoints.get(bpSearchKey);
        if (bpv != null & bpv.hasCommands()) {
            bpv.runCommands();
        }
    }

    /**
     * Check if this breakpoint has been created.
     *
     * @param address 24bit extended (breakpoint) address
     * @return true if breakpoint was found, else false.
     */
    public boolean isCreated(int bpAddress) {
        bpSearchKey.setBpAddress(bpAddress);
        Breakpoint bpv = (Breakpoint) breakPoints.get(bpSearchKey);
        if (bpv != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return <true> if breakpoint will stop Z80 execution. (<false> means that
     * it is a display breakpoint)
     *
     * @param bpAddress 24bit extended address
     * @return true, if breakpoint is defined to stop execution.
     */
    public boolean isStoppable(int bpAddress) {
        bpSearchKey.setBpAddress(bpAddress);

        Breakpoint bpv = (Breakpoint) breakPoints.get(bpSearchKey);
        if (bpv != null && bpv.stop == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * preserve CPU T-States as it is at the moment of the breakpoint
     * @param bpAddress
     * @param cpuTstates 
     */
    public void recordTstates(int bpAddress, long cpuTstates) {
        bpSearchKey.setBpAddress(bpAddress);

        Breakpoint bpv = (Breakpoint) breakPoints.get(bpSearchKey);
        if (bpv != null) {
            bpv.setZ80TstateCount(cpuTstates);
        }        
    }

    /**
     * Get the recorded CPU T-States for specified breakpoint address
     * @param bpAddress
     * @return 
     */
    public long getTstates(int bpAddress) {
        bpSearchKey.setBpAddress(bpAddress);

        Breakpoint bpv = (Breakpoint) breakPoints.get(bpSearchKey);
        if (bpv != null) {
            return bpv.getZ80TstateCount();
        } else {
            return 0;
        }       
    }
    
    /**
     * Return <true> if breakpoint is a single-stop breakpoint.
     *
     * @param bpAddress 24bit extended address
     * @return true, if breakpoint is defined to stop execution.
     */
    public boolean isSingleStopBreakpoint(int bpAddress) {
        bpSearchKey.setBpAddress(bpAddress);

        Breakpoint bpv = (Breakpoint) breakPoints.get(bpSearchKey);
        if (bpv != null && bpv.onestop == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Mark breakpoint as active.
     *
     * @param bpAddress
     */
    public void activate(int bpAddress) {
        bpSearchKey.setBpAddress(bpAddress);

        Breakpoint bpv = (Breakpoint) breakPoints.get(bpSearchKey);
        if (bpv != null) {
            bpv.active = true;
        }
    }

    /**
     * Mark breakpoint as suspended.
     *
     * @param bpAddress
     */
    public void suspend(int bpAddress) {
        bpSearchKey.setBpAddress(bpAddress);

        Breakpoint bpv = (Breakpoint) breakPoints.get(bpSearchKey);
        if (bpv != null) {
            bpv.active = false;
        }
    }

    /**
     * Return <true> if breakpoint is active (<false> if breakpoint is
     * suspended; ie. will be ignored)
     *
     * @param bpAddress 24bit extended address
     * @return true, if breakpoint is defined as active
     */
    public boolean isActive(int bpAddress) {
        bpSearchKey.setBpAddress(bpAddress);

        Breakpoint bpv = (Breakpoint) breakPoints.get(bpSearchKey);
        if (bpv != null && bpv.active == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * List breakpoints into String, so that caller decides to display them.
     */
    public String displayBreakpoints() {
        StringBuffer output = new StringBuffer(1024);
        output.append("Breakpoints:\n");
        if (breakPoints.isEmpty() == true) {
            return new String("No Breakpoints defined.");
        } else {
            Iterator keyIterator = breakPoints.entrySet().iterator();

            while (keyIterator.hasNext()) {
                Map.Entry e = (Map.Entry) keyIterator.next();
                Breakpoint bp = (Breakpoint) e.getKey();

                output.append(Dz.extAddrToHex(bp.getBpAddress(), false));
                output.append(bp.stop == false ? "[d]" : "");
                output.append(bp.active == false ? "[s]" : "");
                output.append("\t");
            }
            output.append("\n");
        }

        return output.toString();
    }

    /**
     * 
     */
    public String setBreakpointStatus(boolean status) {
        if (breakPoints.isEmpty() == true) {
            return new String("No Breakpoints defined.");
        } else {
            Iterator keyIterator = breakPoints.entrySet().iterator();

            while (keyIterator.hasNext()) {
                Map.Entry e = (Map.Entry) keyIterator.next();
                Breakpoint bp = (Breakpoint) e.getKey();
                bp.setActiveStatus(status);
            }
        }

        return displayBreakpoints();
    }
    
    /**
     * List breakpoints into String, that can be saved in a property.<br> Each
     * breakpoint is written in hex, separated with a comma. If a break is a
     * display-breakpoint, it is preceeded with a '[d]'. If no breakpoints are
     * defined, an empty string is returned.
     */
    public String breakpointList() {
        StringBuffer output = new StringBuffer(1024);
        if (breakPoints.isEmpty() == true) {
            return "";
        } else {
            Iterator keyIterator = breakPoints.entrySet().iterator();

            while (keyIterator.hasNext()) {
                Map.Entry e = (Map.Entry) keyIterator.next();
                Breakpoint bp = (Breakpoint) e.getKey();

                output.append(bp.stop == false ? "[d]" : "");
                output.append(Dz.extAddrToHex(bp.getBpAddress(), false));
                if (keyIterator.hasNext() == true) {
                    output.append(",");
                }
            }
        }

        return output.toString();
    }

    /**
     * Set the breakpoint flags in Z88 memory for all currently defined (and
     * active) breakpoints.
     */
    public void installBreakpoints() {
        if (breakPoints.isEmpty() == false) {
            Iterator keyIterator = breakPoints.entrySet().iterator();

            while (keyIterator.hasNext()) {
                Map.Entry e = (Map.Entry) keyIterator.next();
                Breakpoint bp = (Breakpoint) e.getKey();

                Z88.getInstance().getMemory().setBreakpoint(bp.getBpAddress());
            }
        }
    }

    /**
     * Clear the breakpoint flags in Z88 memory.
     */
    public void clearBreakpoints() {
        if (breakPoints.isEmpty() == false) {
            Iterator keyIterator = breakPoints.entrySet().iterator();

            while (keyIterator.hasNext()) {
                Map.Entry e = (Map.Entry) keyIterator.next();
                Breakpoint bp = (Breakpoint) e.getKey();

                Z88.getInstance().getMemory().clearBreakpoint(bp.getBpAddress());
            }
        }
    }

    /**
     * Remove all registered breakpoints within this container (using the
     * displayBreakpoints() method afterwards will return a "No Breakpoints
     * defined." string).<p>
     *
     * This method is typically used when a snapshot is being loaded that might
     * contain a different set of breakpoints; the current set therefore needs
     * to be removed before a new is loaded.
     */
    public void removeBreakPoints() {
        clearBreakpoints(); // first remove breakpoints from memory model..
        breakPoints.clear();
    }

    // The breakpoint container.
    private class Breakpoint {

        private int addressKey;         // the 24bit address of the breakpoint
        private boolean onestop;        // when encountered, this breakpoint is removed
        private boolean stop;           // true = stoppable breakpoint, false = display breakpoint
        private boolean active;         // true = breakpoint is active, false = breakpoint is suspended
        private long z80Tstates = 0;    // the T-State count of the Z80 CPU, when the breakpoint was encountered
        private ArrayList<String> commands;     // array of commands to be executed at breakpoint.

        /**
         * Create a default breakpoint object.
         *
         * @param bpAddress 24bit extended address
         */
        Breakpoint(int bpAddress) {
            stop = true;    // default behaviour is to stop execution at breakpoint
            onestop = false;
            active = true;  // when a breakpoint is created it is active by default

            // the encoded key for the SortedSet...
            addressKey = bpAddress;
            commands = null;
        }

        /**
         * Create a breakpoint object that has debug mode commands
         *
         * @param bpAddress 24bit extended address
         * @param one or more debug commands to be executed (separated by ;)
         */
        Breakpoint(int bpAddress, ArrayList<String> cmds) {
            stop = true;    // default behaviour is to stop execution at breakpoint
            onestop = false; // this is not a one-stop breakpoint
            active = true;  // when a breakpoint is created it is active by default

            // the encoded key for the SortedSet...
            addressKey = bpAddress;
            commands = cmds;
        }

        Breakpoint(int bpAddress, boolean stopAtAddress) {
            // use <false> to display register status, then continue, <true> to stop execution.
            stop = stopAtAddress;
            onestop = false; // this is not a one-stop breakpoint
            active = true;  // when a breakpoint is created it is active by default

            // the encoded key for the SortedSet...
            addressKey = bpAddress;
            commands = null;
        }

        Breakpoint(int bpAddress, boolean stopAtAddress,  boolean singleStop) {
            // use <false> to display register status, then continue, <true> to stop execution.
            stop = stopAtAddress;
            onestop = singleStop; // define one-stop breakpoint, or not
            active = true;  // when a breakpoint is created it is active by default

            // the encoded key for the SortedSet...
            addressKey = bpAddress;
            commands = null;
        }

        private void setBpAddress(int bpAddress) {
            addressKey = bpAddress;
        }

        private int getBpAddress() {
            return addressKey;
        }

        private void setZ80TstateCount(long ts) {
            z80Tstates = ts;
        }

        private void setActiveStatus(boolean status) {
            active = status;
        }
        
        private long getZ80TstateCount() {
            return z80Tstates;
        }
        
        // override interface with the actual implementation for this object.
        public int hashCode() {
            return addressKey;  // the unique key is a perfect hash code
        }

        private boolean hasCommands() {
            if (commands != null) {
                return true;
            } else {
                return false;
            }
        }

        private void runCommands() {
            if (commands != null) {
                CommandLine cmdLine = CommandLine.getInstance();

                DebugGui.getInstance().disableConsoleInput(); // don't allow command input while parsing file...
                for (int i = 0; i < commands.size(); i++) {
                    cmdLine.parseCommandLine(commands.get(i));
                }
                DebugGui.getInstance().enableConsoleInput(); // ready for commands from the keyboard again...
            }
        }

        // override interface with the actual implementation for this object.
        public boolean equals(Object bp) {
            if (!(bp instanceof Breakpoint)) {
                return false;
            }

            Breakpoint aBreakpoint = (Breakpoint) bp;
            if (addressKey == aBreakpoint.addressKey) {
                return true;
            } else {
                return false;
            }
        }
    }
}
