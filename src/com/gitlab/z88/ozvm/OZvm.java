/*
 * OZvm.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:hello@bits4fun.net">Gunther Strube</A>
 * (C) Gunther Strube (hello@bits4fun.net) 2000-2021
 *
 */
package com.gitlab.z88.ozvm;

import com.gitlab.z88.ozvm.datastructures.SlotInfo;
import com.gitlab.z88.ozvm.filecard.FileArea;
import com.gitlab.z88.ozvm.filecard.FileAreaExhaustedException;
import com.gitlab.z88.ozvm.filecard.FileAreaNotFoundException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.net.URI;
import java.util.Properties;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.filechooser.FileSystemView;

/**
 * Main entry of the Z88 virtual machine.
 */
public class OZvm {

    private static final class singletonContainer {
        static final OZvm singleton = new OZvm();
    }

    public static OZvm getInstance() {
        return singletonContainer.singleton;
    }

    /**
     * default boot snapshot filename
     */
    public static final String defaultVmFile = System.getProperty("user.home") + File.separator + "boot.z88";

    public static final String defaultPreferencesFile = System.getProperty("user.home") + File.separator + "ozvm.ini";

    public static final String defaultRstApiCsvFilename = "rstapi.csv";

    private static Properties loadedPreferences = null;

    /**
     * The System ROM definition for slot 0 when OZvm is starting up.
     * Preset to latest OZ ROM release, but may be updated by OZvm preferences
     */
    public static final String systemBootRom = "roms/Z88OZ47.rom";

    /**
     * Variable to hold the default ROM that is inserted into slot 0 when OZvm is starting up.
     * This variable may hold the system ROM or an externally specified ROM from preferences.
     */
    public static String defaultBootRom = "";

    /**
     * The default path + filename of the RST API for the disassembler
     */
    private String defaultRstApiFilename;

    /**
     * (default) boot the virtual machine, once it has been loaded
     */
    private RtmMessageGui rtmMsgGui;
    private String guiKbLayout;
    private Blink blink;
    private Memory memory;
    private Gui gui;
    private File currentRomDir, currentCardDir, currentFilesDir;
    private int consoleFontSize;
    private int dbgWinWidth;
    private int dbgWinHeight;
    private boolean autoRun;
    private boolean displayZ88CardSlots;
    private boolean displayRubberKeyboard;
    private boolean emulate3200hzBeeper;    

    private OZvm() {
        autoRun = true; // default autorun...
        displayZ88CardSlots = true;
        displayRubberKeyboard = true;
        emulate3200hzBeeper = true;

        guiKbLayout = ""; // use default Gui settings, if neither command line nor snapshot specifies it
        consoleFontSize = 11; // the default "Monochrome" font size for Debug Console
        dbgWinWidth = 960; // the default dimension of Debug Gui
        dbgWinHeight = 480;

        // where is the default RST API CSV file, for disassembler to load it
        defaultRstApiFilename = getAppPath() + defaultRstApiCsvFilename;

        FileSystemView filesys = FileSystemView.getFileSystemView();
        currentRomDir = currentCardDir = currentFilesDir = filesys.getHomeDirectory();

        blink = Z88.getInstance().getBlink();
        memory = Z88.getInstance().getMemory();
        Z88.getInstance().getDisplay().start();

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                // Save current OZvm settings before quitting application
                OZvm.savePreferences();
            }
        });
    }
    
    public void set3200hzBeeperEmulation(boolean bp3200hz)  {
        emulate3200hzBeeper = bp3200hz;
    }

    public boolean get3200hzBeeperEmulation()  {
        return emulate3200hzBeeper;
    }
    
    public boolean getDisplayZ88CardSlots() {
        return displayZ88CardSlots;
    }

    public void setDisplayZ88CardSlots(boolean displayZ88CardSlots) {
        this.displayZ88CardSlots = displayZ88CardSlots;
    }
    
    public boolean getDisplayRubberKeyboard() {
        return displayRubberKeyboard;
    }

    public void setDisplayRubberKeyboard(boolean displayRubberKeyboard) {
        this.displayRubberKeyboard = displayRubberKeyboard;
    }
    
    public void setConsoleFontSize(int fontSize) {
        consoleFontSize = fontSize;
    }

    /**
     * The default Debug Console font size
     */
    public int getConsoleFontSize() {
        return consoleFontSize;
    }

    public void setDebugWinWidth(int winwidth) {
        dbgWinWidth = winwidth;
    }

    public void setDebugWinHeight(int winheight) {
        dbgWinHeight = winheight;
    }

    public int getDebugWinWidth() {
        return dbgWinWidth;
    }

    public int getDebugWinHeight() {
        return dbgWinHeight;
    }

    public void setRstApiCsvFileName(String rstApiCsvFileName) {
        defaultRstApiFilename = rstApiCsvFileName;
    }

    public String getRstApiCsvFileName() {
        return defaultRstApiFilename;
    }

    public boolean getAutorunStatus() {
        return autoRun;
    }

    /**
     * Get a reference to the main graphical user interface.
     *
     * @return
     */
    public Gui getGui() {
        return gui;
    }

    public File getCurrentRomDir() {
        return currentRomDir;
    }

    public void setCurrentRomDir(File romDir) {
        currentRomDir = romDir;
    }

    public File getCurrentCardDir() {
        return currentCardDir;
    }

    public void setCurrentCardDir(File cardDir) {
        currentCardDir = cardDir;
    }

    public File getCurrentFilesDir() {
        return currentFilesDir;
    }

    public void setCurrentFilesDir(File filesDir) {
        currentFilesDir = filesDir;
    }

    /**
     * Boot OZvm and parse operating system shell command line arguments
     *
     * @param args
     * @return
     */
    public boolean boot(String[] args) {
        RandomAccessFile file;
        boolean loadedRom = false;
        boolean installedCard = false;
        boolean loadedSnapshot = false;
        boolean ramSlot0 = false;
        int ramSizeArg = 0, eprSizeArg = 0, ram0Kb = 128;

        gui = new Gui(); // instantiated but not yet displayed...

        try {
            int arg = 0;
            while (arg < args.length) {
                if (args[arg].compareTo("rom") != 0
                        & args[arg].compareTo("stm0") != 0 & args[arg].compareTo("amd0") != 0
                        & args[arg].compareTo("ram0") != 0 & args[arg].compareTo("ram1") != 0
                        & args[arg].compareTo("ram2") != 0 & args[arg].compareTo("ram3") != 0
                        & args[arg].compareTo("epr1") != 0 & args[arg].compareTo("epr2") != 0 & args[arg].compareTo("epr3") != 0
                        & args[arg].compareTo("fcd1") != 0 & args[arg].compareTo("fcd2") != 0 & args[arg].compareTo("fcd3") != 0
                        & args[arg].compareTo("crd1") != 0 & args[arg].compareTo("crd2") != 0 & args[arg].compareTo("crd3") != 0
                        & args[arg].compareTo("kbl") != 0 & args[arg].compareTo("debug") != 0 & args[arg].compareTo("initdebug") != 0) {
                    // try to install specified snapshot file
                    SaveRestoreVM srVm = new SaveRestoreVM();
                    String vmFileName = args[arg];
                    if (vmFileName.toLowerCase().lastIndexOf(".z88") == -1) {
                        vmFileName += ".z88"; // '.z88' extension was missing.
                    }
                    try {
                        autoRun = srVm.loadSnapShot(vmFileName);
                        displayRtmMessage("Snapshot successfully installed from " + vmFileName);
                        gui.setWindowTitle("[" + (new File(vmFileName).getName()) + "]");
                        loadedSnapshot = true;
                    } catch (IOException ee) {
                        // loading of snapshot failed (file not found, corrupt or not a snapshot file
                        // define a default Z88 system as fall back plan.
                        memory.setDefaultSystem();
                        Z88.getInstance().getProcessor().reset();
                        blink.resetBlink();
                    }
                    arg++;
                }

                if (arg < args.length && (args[arg].compareTo("stm0") == 0)) {
                    memory.setRomSlotFlashType(SlotInfo.StmFlashCard);
                    arg++;
                    continue;
                }

                if (arg < args.length && (args[arg].compareTo("amd0") == 0)) {
                    memory.setRomSlotFlashType(SlotInfo.AmdFlashCard);
                    arg++;
                    continue;
                }

                if (arg < args.length && (args[arg].compareTo("rom") == 0)) {
                    displayRtmMessage("Loading '" + args[arg + 1] + "' into ROM space   in slot 0.");
                    File romFile = new File(args[arg + 1]);
                    memory.loadRomBinary(romFile);
                    gui.setWindowTitle("[" + (romFile.getName()) + "]");
                    blink.resetBlink();
                    blink.setRAMS(memory.getBank(0));   // point at ROM bank 0
                    loadedRom = true;
                    arg += 2;
                }

                if (arg < args.length && (args[arg].startsWith("ram") == true)) {
                    int ramSlotNumber = args[arg].charAt(3) - 48;
                    ramSizeArg = Integer.parseInt(args[arg + 1], 10);
                    if (ramSlotNumber == 0) {
                        if ((ramSizeArg < 32) | (ramSizeArg > 512)) {
                            displayRtmMessage("Only 32K-512K RAM Card size allowed in slot " + ramSlotNumber);
                            return false;
                        }
                    } else {
                        if ((ramSizeArg < 32) | (ramSizeArg > 1024)) {
                            displayRtmMessage("Only 32K-1024K RAM Card size allowed in slot " + ramSlotNumber);
                            return false;
                        }
                    }
                    memory.insertRamCard(ramSizeArg, ramSlotNumber);    // RAM Card specified for slot x...
                    if (ramSlotNumber == 0) {
                        ramSlot0 = true;
                    }
                    displayRtmMessage("Inserted " + ramSizeArg + "K RAM Card in slot " + ramSlotNumber);
                    installedCard = true;

                    arg += 2;
                    continue;
                }

                if (arg < args.length && (args[arg].startsWith("epr") == true)) {
                    int eprSlotNumber = args[arg].charAt(3) - 48;
                    eprSizeArg = Integer.parseInt(args[arg + 1], 10);
                    String insertEprMsg = "";
                    int eprType = 0;
                    if (args[arg + 2].compareToIgnoreCase("27C") == 0) {
                        insertEprMsg = "Inserted " + eprSizeArg + "K UV Eprom Card in slot " + eprSlotNumber;
                        eprType = SlotInfo.EpromCard;
                    }
                    if (args[arg + 2].compareToIgnoreCase("28F") == 0) {
                        insertEprMsg = "Inserted " + eprSizeArg + "K Intel Flash Card in slot " + eprSlotNumber;
                        eprType = SlotInfo.IntelFlashCard;
                    }
                    if (args[arg + 2].compareToIgnoreCase("29F") == 0) {
                        insertEprMsg = "Inserted " + eprSizeArg + "K Amd Flash  Card in slot " + eprSlotNumber;
                        eprType = SlotInfo.AmdFlashCard;
                    }
                    if (memory.insertEprCard(eprSlotNumber, eprSizeArg, eprType) == true) {
                        displayRtmMessage(insertEprMsg);
                        installedCard = true;
                    } else {
                        displayRtmMessage("Eprom Card size/type configuration is illegal.");
                    }
                    arg += 3;
                    continue;
                }

                if (arg < args.length && (args[arg].startsWith("fcd") == true)) {
                    int eprSlotNumber = args[arg].charAt(3) - 48;
                    eprSizeArg = Integer.parseInt(args[arg + 1], 10);
                    String insertEprMsg = "";
                    int eprType = 0;
                    if (args[arg + 2].compareToIgnoreCase("27C") == 0) {
                        insertEprMsg = "Inserted " + eprSizeArg + "K UV Eprom Card in slot " + eprSlotNumber;
                        eprType = SlotInfo.EpromCard;
                    }
                    if (args[arg + 2].compareToIgnoreCase("28F") == 0) {
                        insertEprMsg = "Inserted " + eprSizeArg + "K Intel Flash Card in slot " + eprSlotNumber;
                        eprType = SlotInfo.IntelFlashCard;
                    }
                    if (args[arg + 2].compareToIgnoreCase("29F") == 0) {
                        insertEprMsg = "Inserted " + eprSizeArg + "K Amd Flash  Card in slot " + eprSlotNumber;
                        eprType = SlotInfo.AmdFlashCard;
                    }
                    if (memory.insertFileCard(eprSlotNumber, eprSizeArg, eprType) == true) {
                        displayRtmMessage(insertEprMsg);
                        installedCard = true;
                    } else {
                        displayRtmMessage("Eprom File Card size/type configuration is illegal.");
                    }
                    arg += 3;

                    if (arg < args.length) {
                        FileArea fa = new FileArea(eprSlotNumber);
                        // optional: import files from host system into file area.
                        while (args[arg].compareToIgnoreCase("-d") == 0 | args[arg].compareToIgnoreCase("-f") == 0) {
                            if (args[arg].compareToIgnoreCase("-f") == 0) {
                                fa.importHostFile(new File(args[arg + 1]));
                            }
                            if (args[arg].compareToIgnoreCase("-d") == 0) {
                                fa.importHostFiles(new File(args[arg + 1]));
                            }
                            arg += 2;
                            if (arg >= args.length) {
                                break;
                            }
                        }
                    }
                    continue;
                }

                if (arg < args.length && (args[arg].startsWith("crd") == true)) {
                    int eprSlotNumber = args[arg].charAt(3) - 48;
                    eprSizeArg = Integer.parseInt(args[arg + 1], 10);
                    String insertEprMsg = "";
                    int eprType = 0;
                    if (args[arg + 2].compareToIgnoreCase("27C") == 0) {
                        insertEprMsg = "Loaded  file image '" + args[arg + 3] + "' on " + eprSizeArg + "K   UV Eprom Card in slot " + eprSlotNumber;
                        eprType = SlotInfo.EpromCard;
                    }
                    if (args[arg + 2].compareToIgnoreCase("28F") == 0) {
                        insertEprMsg = "Loaded  file image '" + args[arg + 3] + "' on " + eprSizeArg + "K   Intel Flash Card in slot " + eprSlotNumber;
                        eprType = SlotInfo.IntelFlashCard;
                    }
                    if (args[arg + 2].compareToIgnoreCase("29F") == 0) {
                        insertEprMsg = "Loaded  file image '" + args[arg + 3] + "' on " + eprSizeArg + "K   Amd Flash Card in slot " + eprSlotNumber;
                        eprType = SlotInfo.AmdFlashCard;
                    }

                    if (args[arg + 3].compareToIgnoreCase("-b") == 0) {
                        memory.loadBankFilesOnCard(eprSlotNumber, eprSizeArg, eprType, args[arg + 4]);
                        installedCard = true;
                        arg += 5;
                    } else {
                        memory.loadFileImageOnCard(eprSlotNumber, eprSizeArg, eprType, new File(args[arg + 3]));
                        displayRtmMessage(insertEprMsg);
                        installedCard = true;
                        arg += 4;
                    }
                    continue;
                }

                if (arg < args.length && (args[arg].compareTo("debug") == 0)) {
                    autoRun = false;
                    DebugGui.getInstance().activateDebugCommandLine();
                    arg++;
                    continue;
                }

                if (arg < args.length && (args[arg].compareTo("initdebug") == 0)) {
                    autoRun = false;
                    DebugGui.getInstance().activateDebugCommandLine();

                    file = new RandomAccessFile(args[arg + 1], "r");
                    DebugGui.getInstance().disableConsoleInput();  // don't allow command input while parsing file...
                    String cmd;
                    while ((cmd = file.readLine()) != null) {
                        CommandLine.getInstance().parseCommandLine(cmd);
                    }
                    DebugGui.getInstance().enableConsoleInput();; // ready for commands from the keyboard...
                    file.close();
                    displayRtmMessage("Parsed '" + args[arg + 1] + "' command file.");

                    arg += 2;
                    continue;
                }

                if (arg < args.length && (args[arg].compareTo("kbl") == 0)) {
                    if (args[arg + 1].compareToIgnoreCase("uk") == 0 || args[arg + 1].compareToIgnoreCase("en") == 0) {
                        guiKbLayout = "uk";
                    }
                    if (args[arg + 1].compareTo("fr") == 0) {
                        guiKbLayout = "fr";
                    }
                    if (args[arg + 1].compareTo("dk") == 0) {
                        guiKbLayout = "dk";
                    }
                    if (args[arg + 1].compareTo("se") == 0 | args[arg + 1].compareTo("fi") == 0) {
                        guiKbLayout = "se";
                    }

                    arg += 2;
                    continue;
                }
            }

            // always load preferences before snapshot
            loadPreferences();
            
            // all operating system shell command options parsed,
            // if no snapshot file was specified or other resources installed, try to load the default 'boot.z88' snapshot
            if (loadedSnapshot == false & installedCard == false & loadedRom == false) {
                SaveRestoreVM srVm = new SaveRestoreVM();
                try {
                    autoRun = srVm.loadSnapShot(OZvm.defaultVmFile);
                    loadedSnapshot = true;
                    displayRtmMessage("Snapshot successfully installed from " + OZvm.defaultVmFile);
                    gui.setWindowTitle("[boot.z88]");
                    Z88.getInstance().getDisplay().setScreenMessage("");
                } catch (IOException ee) {
                    // 'boot.z88' wasn't available, or an error occurred - ignore it...
                    loadedSnapshot = false;
                }
            }

            if (loadedSnapshot == false && ramSlot0 == false) {
                if (loadedPreferences != null) {
                    if (loadedPreferences.getProperty("Slot0Ram") != null) {
                        ram0Kb = Integer.parseInt(loadedPreferences.getProperty("Slot0Ram"));
                    }
                }

                displayRtmMessage("RAM0 set to default " + ram0Kb + "K.");
                memory.insertRamCard(ram0Kb, 0);   // no RAM specified for slot 0, set to default RAM from preferences...
            }

            if (loadedSnapshot == false & loadedRom == false & defaultBootRom != "") {
                displayRtmMessage("Loading ROM image as specified in preferences: " + defaultBootRom);
                File rf = new File(defaultBootRom);
                if(loadedPreferences != null && loadedPreferences.getProperty("Slot0Type") != null) {
                    int rom0type = Integer.parseInt(loadedPreferences.getProperty("Slot0Type"));
                    memory.loadRomBinary(rf, rom0type);
                    switch(rom0type) {
                        case SlotInfo.AmdFlashCard:
                            OZvm.getInstance().getGui().getAmd512KRomTypeMenuItem().setSelected(true);
                            break;
                        case SlotInfo.StmFlashCard:
                            OZvm.getInstance().getGui().getStm512KRomTypeMenuItem().setSelected(true);
                            break;
                        case SlotInfo.SstFlashCard:
                            OZvm.getInstance().getGui().getSst512KRomTypeMenuItem().setSelected(true);
                            break;
                    }
                } else {
                    memory.loadRomBinary(rf);
                }
                blink.setRAMS(memory.getBank(0));   // point at ROM bank 0
                loadedRom = true;
            }

            if (loadedSnapshot == false & loadedRom == false) {
                displayRtmMessage("No external ROM image specified, using " + systemBootRom);
                File rf = new File(URI.create("file:///" + getAppPath() + systemBootRom.replace("\\","/")));
                memory.loadRomBinary(rf);
                blink.setRAMS(memory.getBank(0));   // point at ROM bank 0
            }
        } catch (FileNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Couldn't load ROM/EPROM image:\n" + e.getMessage() + "\n", "OZvm startup", JOptionPane.ERROR_MESSAGE);
            String romNotFoundPath = e.getMessage();
            if (romNotFoundPath.toLowerCase().contains(systemBootRom.toLowerCase()) == false) {
                JOptionPane.showMessageDialog(null, "Loading System default ROM image:\n" + getAppPath() + systemBootRom + "\n", "OZvm startup", JOptionPane.INFORMATION_MESSAGE);
                // An externally specified ROM failed to load, fall-back to system ROM
                File rf = new File(URI.create("file:///" + getAppPath() + systemBootRom.replace("\\","/")));
                try {
                    memory.loadRomBinary(rf);
                } catch (IOException ex) {
                    return false;
                }
                catch (IllegalArgumentException ex) {
                    return false;
                }
                blink.setRAMS(memory.getBank(0));   // point at ROM bank 0, then continue to load
            } else {
                return false;
            }
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Problem with ROM/EPROM image or I/O:\n" + e.getMessage(), "OZvm startup", JOptionPane.ERROR_MESSAGE);
            return false;
        } catch (FileAreaNotFoundException e) {
            JOptionPane.showMessageDialog(null, "Problem with importing file into File Card:\n" + e.getMessage(), "OZvm startup", JOptionPane.ERROR_MESSAGE);
            return false;
        } catch (FileAreaExhaustedException e) {
            JOptionPane.showMessageDialog(null, "Problem with importing file into File Card:\n" + e.getMessage(), "OZvm startup", JOptionPane.ERROR_MESSAGE);
            return false;
        }

        Z88.getInstance().getDisplay().setBlinkLcdResolution();

        gui.pack(); // update the application UI
        gui.centerGui();

        renderGui();

        gui.setVisible(true);

        return true;
    }

    public String getAppPath() {
        return getClass().getProtectionDomain().getCodeSource().getLocation().getPath().replaceFirst("z88.jar", "");
    }

    private void renderGui() {
        gui.displayZ88ScreenPanel(true);

        gui.displayRunTimeMessagesPane(false);  // default invisible runtime message window.
        gui.redrawGuiWindows(gui.getScreenDoubleSizeMenuItem().getState());
        gui.getSlotsPanel().refreshSlotInfo();  // render the current slot details on the buttons

        if (guiKbLayout.compareToIgnoreCase("uk") == 0) {
            Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
            gui.getUkLayoutMenuItem().setState(true);
            RubberKeyboard.getInstance().setKeyboardCountrySpecificIcons(Z88Keyboard.COUNTRY_UK);
            displayRtmMessage("Using English (UK) keyboard layout.");
        }
        if (guiKbLayout.compareTo("fr") == 0) {
            Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_FR);
            gui.getUkLayoutMenuItem().setState(true);
            RubberKeyboard.getInstance().setKeyboardCountrySpecificIcons(Z88Keyboard.COUNTRY_FR);
            displayRtmMessage("Using French keyboard layout.");
        }
        if (guiKbLayout.compareTo("dk") == 0) {
            Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_DK);
            gui.getUkLayoutMenuItem().setState(true);
            RubberKeyboard.getInstance().setKeyboardCountrySpecificIcons(Z88Keyboard.COUNTRY_DK);
            displayRtmMessage("Using Danish keyboard layout.");
        }
        if (guiKbLayout.compareTo("se") == 0) {
            Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_SE);
            gui.getUkLayoutMenuItem().setState(true);
            RubberKeyboard.getInstance().setKeyboardCountrySpecificIcons(Z88Keyboard.COUNTRY_SE);
            displayRtmMessage("Using Swedish/Finish keyboard layout.");
        }

        gui.setLocationRelativeTo(null);
    }

    public static void savePreferences() {
        int Slot0Ram = Z88.getInstance().getMemory().getInternalRamSize();

        Properties properties = new Properties();

        // V1.2 remember the current Z88 keyboard layout
        properties.setProperty("Z88KbLayout", "" + Z88.getInstance().getKeyboard().getKeyboardLayout());

        // V1.2, remember Z88 screen double size state
        properties.setProperty("ScreenDoubleSize", Boolean.toString(OZvm.getInstance().getGui().getScreenDoubleSizeMenuItem().getState()));

        // V1.2, remember the Z80 CPU speed
        properties.setProperty("CpuRealSpeed", Boolean.toString(OZvm.getInstance().getGui().getZ80CpuRealSpeedMenuItem().isSelected()));

        // V1.2, remember the Display Rubber Keyboard visibility
        properties.setProperty("DisplayRubberKeyboard", Boolean.toString(OZvm.getInstance().getDisplayRubberKeyboard()));
        
        // V1.2, remember the Screen Frames per second rendering setting (the index, 0-3)
        properties.setProperty("ScreenFps", "" + Z88.getInstance().getDisplay().getCurrentFrameRate());

        // V1.2, remember the slot 0 RAM size in Kb
        if (Slot0Ram == 1) {
            Slot0Ram = 8; // if no RAM has been defined for slot 0, set default 128K
        }
        properties.setProperty("Slot0Ram", "" + Slot0Ram * 16);

        // V1.2, remember filename of Rom loaded into slot 0, default latest OZ ROM release
        String slot0RomFilename = OZvm.defaultBootRom;
        if (Z88.getInstance().getMemory().getLoadedRomFile() != null) {
             slot0RomFilename = Z88.getInstance().getMemory().getLoadedRomFile().getAbsolutePath();
        }
        properties.setProperty("Slot0Rom", slot0RomFilename);
        properties.setProperty("Slot0Type", "" + SlotInfo.getInstance().getCardType(0));

        // V1.3, remember Z88 LCD size
        properties.setProperty("BlinkScreenWidth", "" + Z88.getInstance().getBlink().getSCW());
        properties.setProperty("BlinkScreenHeight", "" + Z88.getInstance().getBlink().getSCH());

        // V1.3, remember default selected slot 0 ROM path
        properties.setProperty("Slot0Path", "" + OZvm.getInstance().getCurrentRomDir());

        // V1.3, remember default selected Card image path
        properties.setProperty("CardPath", "" + OZvm.getInstance().getCurrentCardDir());

        // V1.3, remember default selected Imp/Export files path
        properties.setProperty("FilesPath", "" + OZvm.getInstance().getCurrentFilesDir());

        // V1.3, remember Debug Console Font size
        properties.setProperty("ConsoleFontSize", "" + OZvm.getInstance().getConsoleFontSize());

        // V1.3, remember Card Slot Gui availability
        properties.setProperty("DisplayZ88CardSlots", "" + Boolean.toString(OZvm.getInstance().getDisplayZ88CardSlots()));

        // V1.3 remember Debug Window Size
        properties.setProperty("DbgWinWidth", "" + OZvm.getInstance().getDebugWinWidth());
        properties.setProperty("DbgWinHeight", "" + OZvm.getInstance().getDebugWinHeight());

        // V1.3, remember the CSV filename of the RST API
        properties.setProperty("RstApiCsvFile", OZvm.getInstance().getRstApiCsvFileName());

        // V1.3, remember the 3200Hz Beeper emulation option (enabled or disabled)
        properties.setProperty("Beeper3200hz", Boolean.toString(OZvm.getInstance().get3200hzBeeperEmulation()));

        
        File pf = new File(defaultPreferencesFile);
        FileOutputStream pfs;
        try {
            pfs = new FileOutputStream(pf);
            properties.store(pfs, null);
            pfs.close();
        } catch (IOException ex) {}
    }

    public static void loadPreferences() {
        loadedPreferences = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream(defaultPreferencesFile);
            // load a loadedPreferences file
            loadedPreferences.load(input);

        } catch (IOException ex) {
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }
        }

        if (loadedPreferences.getProperty("Z88KbLayout") != null) {
            int kbLayoutCountryCode = Integer.parseInt(loadedPreferences.getProperty("Z88KbLayout"));
            switch (kbLayoutCountryCode) {
                case Z88Keyboard.COUNTRY_UK:
                    Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                    OZvm.getInstance().getGui().getUkLayoutMenuItem().setState(true);
                    RubberKeyboard.getInstance().setKeyboardCountrySpecificIcons(Z88Keyboard.COUNTRY_UK);
                    break;
                case Z88Keyboard.COUNTRY_DK:
                    Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_DK);
                    RubberKeyboard.getInstance().setKeyboardCountrySpecificIcons(Z88Keyboard.COUNTRY_DK);
                    OZvm.getInstance().getGui().getDkLayoutMenuItem().setState(true);
                    break;
                case Z88Keyboard.COUNTRY_FR:
                    Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_FR);
                    RubberKeyboard.getInstance().setKeyboardCountrySpecificIcons(Z88Keyboard.COUNTRY_FR);
                    OZvm.getInstance().getGui().getFrLayoutMenuItem().setState(true);
                    break;
                case Z88Keyboard.COUNTRY_SE: // Swedish/Finish
                    Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_SE);
                    RubberKeyboard.getInstance().setKeyboardCountrySpecificIcons(Z88Keyboard.COUNTRY_SE);
                    OZvm.getInstance().getGui().getSeLayoutMenuItem().setState(true);
                    break;
                default:
                    // all other keyboard layouts are default UK (since they're not implemented yet)
                    Z88.getInstance().getKeyboard().setKeyboardLayout(Z88Keyboard.COUNTRY_UK);
                    OZvm.getInstance().getGui().getUkLayoutMenuItem().setState(true);
            }
        }

        // V1.2, restore filename of Rom loaded into slot 0, if specified
        if (loadedPreferences.getProperty("Slot0Rom") != null) {
            String slot0RomFilename = loadedPreferences.getProperty("Slot0Rom");
            if (slot0RomFilename != "") {
                defaultBootRom = slot0RomFilename;
            }
        }

        if (loadedPreferences.getProperty("DisplayZ88CardSlots") != null) {
            boolean dispZ88CrdSlots = Boolean.valueOf(loadedPreferences.getProperty("DisplayZ88CardSlots")).booleanValue();
            OZvm.getInstance().setDisplayZ88CardSlots(dispZ88CrdSlots);
            OZvm.getInstance().getGui().getShowSlotsMenuItem().setSelected(dispZ88CrdSlots);
        }

        // V1.2, restore the Display Rubber Keyboard visibility setting
        if (loadedPreferences.getProperty("DisplayRubberKeyboard") != null) {
            boolean displayRubberKeyboard = Boolean.valueOf(loadedPreferences.getProperty("DisplayRubberKeyboard")).booleanValue();
            OZvm.getInstance().setDisplayRubberKeyboard(displayRubberKeyboard);
            OZvm.getInstance().getGui().getShowRubberKbMenuItem().setSelected(displayRubberKeyboard);
        }

        // OZvm V1.2, restore CPU Real Speed setting
        if (loadedPreferences.getProperty("CpuRealSpeed") != null) {
            boolean cpuRealSpeed = Boolean.valueOf(loadedPreferences.getProperty("CpuRealSpeed")).booleanValue();
            OZvm.getInstance().getGui().getZ80CpuRealSpeedMenuItem().setSelected(cpuRealSpeed);
            Z88.getInstance().getProcessor().setCpuRealSpeed(cpuRealSpeed);
        }

        // V1.2, restore the Screen Frames per second rendering setting (the index, 0-3)
        if (loadedPreferences.getProperty("ScreenFps") != null) {
            int screenFps = Integer.parseInt(loadedPreferences.getProperty("ScreenFps"));
            Z88.getInstance().getDisplay().setFrameRate(screenFps);
            switch (screenFps) {
                case 0:
                    OZvm.getInstance().getGui().getScreen10FpsMenuItem().setSelected(true);
                    break;
                case 1:
                    OZvm.getInstance().getGui().getScreen25FpsMenuItem().setSelected(true);
                    break;
                case 2:
                    OZvm.getInstance().getGui().getScreen50FpsMenuItem().setSelected(true);
                    break;
                case 3:
                    OZvm.getInstance().getGui().getScreen100FpsMenuItem().setSelected(true);
                    break;
            }
        }

        // V1.3, restore Z88 LCD size
        if (loadedPreferences.getProperty("BlinkScreenWidth") != null && loadedPreferences.getProperty("BlinkScreenHeight") != null) {
            int lcdScrWidth = Integer.parseInt(loadedPreferences.getProperty("BlinkScreenWidth")) * 8;
            int lcdScrHeight = Integer.parseInt(loadedPreferences.getProperty("BlinkScreenHeight")) * 8;

            Z88.getInstance().getBlink().setSCW(lcdScrWidth/8);
            Z88.getInstance().getBlink().setSCH(lcdScrHeight/8);

            if (lcdScrWidth == 640 && lcdScrHeight == 64) OZvm.getInstance().getGui().getScreen640x64MenuItem().setSelected(true);
            if (lcdScrWidth == 640 && lcdScrHeight == 320) OZvm.getInstance().getGui().getScreen640x320MenuItem().setSelected(true);
            if (lcdScrWidth == 640 && lcdScrHeight == 480) OZvm.getInstance().getGui().getScreen640x480MenuItem().setSelected(true);
            if (lcdScrWidth == 800 && lcdScrHeight == 320) OZvm.getInstance().getGui().getScreen800x320MenuItem().setSelected(true);
            if (lcdScrWidth == 800 && lcdScrHeight == 480) OZvm.getInstance().getGui().getScreen800x480MenuItem().setSelected(true);
        } else {
            // these registers were not defined in snapshot file, default to 640x64 resolution
            Z88.getInstance().getBlink().setSCW(640/8);
            Z88.getInstance().getBlink().setSCH(64/8);
            OZvm.getInstance().getGui().getScreen640x64MenuItem().setSelected(true);
        }

        // OZvm V1.2, restore Double Screen setting
        if (loadedPreferences.getProperty("ScreenDoubleSize") != null) {
            boolean screenDoubleSize = Boolean.valueOf(loadedPreferences.getProperty("ScreenDoubleSize")).booleanValue();
            OZvm.getInstance().getGui().getScreenDoubleSizeMenuItem().setState(screenDoubleSize);
        } else {
            // Get default screen double size
            boolean screenDoubleSize = Z88.getInstance().getDisplay().isDoubleScreenSize();
            OZvm.getInstance().getGui().getScreenDoubleSizeMenuItem().setState(screenDoubleSize);
        }
        
        // V1.3, set default slot 0 ROM path
        if (loadedPreferences.getProperty("Slot0Path") != null) {
            File curRomDir = new File(loadedPreferences.getProperty("Slot0Path"));
            OZvm.getInstance().setCurrentRomDir(curRomDir);
        }

        // V1.3, set default selected Card image path
        if (loadedPreferences.getProperty("CardPath") != null) {
            File curPathDir = new File(loadedPreferences.getProperty("CardPath"));
            OZvm.getInstance().setCurrentCardDir(curPathDir);
        }

        // V1.3, set default selected Imp/Export files path
        if (loadedPreferences.getProperty("FilesPath") != null) {
            File curFilesDir = new File(loadedPreferences.getProperty("FilesPath"));
            OZvm.getInstance().setCurrentFilesDir(curFilesDir);
        }

        // V1.3, set Debug Console Font size (default 11)
        if (loadedPreferences.getProperty("ConsoleFontSize") != null) {
            int fontSize = Integer.parseInt(loadedPreferences.getProperty("ConsoleFontSize"));
            OZvm.getInstance().setConsoleFontSize(fontSize);
        }

        // V1.3, set Debug Console Window Width
        if (loadedPreferences.getProperty("DbgWinWidth") != null) {
            int winwidth = Integer.parseInt(loadedPreferences.getProperty("DbgWinWidth"));
            OZvm.getInstance().setDebugWinWidth(winwidth);
        }

        // V1.3, set Debug Console Window Height
        if (loadedPreferences.getProperty("DbgWinHeight") != null) {
            int winheight = Integer.parseInt(loadedPreferences.getProperty("DbgWinHeight"));
            OZvm.getInstance().setDebugWinHeight(winheight);
        }

        // V1.3, Get the CSV filename of the RST API, if specified
        if (loadedPreferences.getProperty("RstApiCsvFile") != null) {
            String rstApiCsvFileName = loadedPreferences.getProperty("RstApiCsvFile");
            if (rstApiCsvFileName != "") {
                OZvm.getInstance().setRstApiCsvFileName(rstApiCsvFileName);
            }
        }
        
        // V1.3, Enable/Disable 3200Hz Beeper sound emulation
        if (loadedPreferences.getProperty("Beeper3200hz") != null) {
            boolean beeper3200hzOption = Boolean.valueOf(loadedPreferences.getProperty("Beeper3200hz")).booleanValue();
            OZvm.getInstance().set3200hzBeeperEmulation(beeper3200hzOption);
            OZvm.getInstance().getGui().getBeeper3200hzMenuItem().setSelected(beeper3200hzOption);
        }        
    }

    public static void displayRtmMessage(final String msg) {
        OZvm.getInstance().getRtmMsgGui().displayRtmMessage(msg);
    }

    /**
     * OZvm application startup.
     *
     * @param args shell command line arguments
     */
    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        } catch (Exception e1) {
            System.out.println("Error setting cross platform LAF: " + e1);
        }

        OZvm ozvm = OZvm.getInstance();

        if (ozvm.boot(args) == false) {
            System.out.println("Ozvm terminated.");
            System.exit(0);
        }

        if (ozvm.getAutorunStatus() == true) {
            // no debug mode, just boot the specified ROM and run the virtual Z88...
            Z88.getInstance().runZ80Cpu();
            // make sure that keyboard focus is available for Z88 (screen)
            Z88.getInstance().getDisplay().grabFocus();
        } else {
            DebugGui.getInstance().activateDebugCommandLine();
            CommandLine.getInstance().cmdlineFirstSingleStep();
        }
    }

    public RtmMessageGui getRtmMsgGui() {
        if (rtmMsgGui == null) {
            rtmMsgGui = new RtmMessageGui();
        }

        return rtmMsgGui;
    }
}
