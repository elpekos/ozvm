/*
 * Watchpoints.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:hello@bits4fun.net">Gunther Strube</A>
 * (C) Gunther Strube (hello@bits4fun.net) 2000-2021
 *
 */
package com.gitlab.z88.ozvm;

import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Manage watchpoint addresses in Z88 virtual machine.
 */
public class Watchpoints {

    private Map watchPoints;
    private Watchpoint bpSearchKey;

    /**
     * Just instantiate this Watchpoint Manager
     */
    public Watchpoints() {
        watchPoints = new HashMap();
        bpSearchKey = new Watchpoint(0);    // just create a dummy search key object (used by internal lookup)
    }

    /**
     * Remove Read/Write watchpoint, if created
     *
     * @param wpAddress 24bit extended address
     */
    public void clearWatchpoint(int wpAddress) {
        Watchpoint bp = new Watchpoint(wpAddress);
        if (watchPoints.containsKey(bp) == true) {
            watchPoints.remove(bp);
            Z88.getInstance().getMemory().clearWatchpoint(wpAddress);
        }
    }

    /**
     * Add (if not created) a read/write watchpoint
     *
     * @param wpAddress 24bit extended address
     */
    public void setWatchpoint(int wpAddress) {
        Watchpoint wp = new Watchpoint(wpAddress);
        if (watchPoints.containsKey(wp) == false) {
            watchPoints.put(wp, wp);

            Z88.getInstance().getMemory().setReadWatchpoint(wpAddress);
            Z88.getInstance().getMemory().setReadWatchpoint(wpAddress);
        }
    }

    /**
     * Add (if not created) or remove Read/Write watchpoint (if previously created).
     *
     * @param wpAddress 24bit extended address
     */
    public void toggleWatchpoint(int wpAddress) {
        Watchpoint bp = new Watchpoint(wpAddress);
        if (watchPoints.containsKey(bp) == false) {
            watchPoints.put(bp, bp);
            Z88.getInstance().getMemory().setWatchpoint(wpAddress);
        } else {
            watchPoints.remove(bp);
            Z88.getInstance().getMemory().clearWatchpoint(wpAddress);
        }
    }

    /**
     * Add (if not created) or remove Read watchpoint (if previously created).
     * A read watchpoint is only removed if both read & write signals are
     * disabled.
     *
     * @param wpAddress 24bit extended address
     */
    public void toggleReadWatchpoint(int wpAddress) {
        bpSearchKey.setWpAddress(wpAddress);
        Watchpoint bpv = (Watchpoint) watchPoints.get(bpSearchKey);
        if (bpv != null) {
            Z88.getInstance().getMemory().clearReadWatchpoint(wpAddress);
            if (bpv.databusWrite == true) {
                // don't delete watchpoint, it is enabled for write
                bpv.databusRead = false;
            } else {
                // remove watchpoint, and clear read/write watches in memory
                watchPoints.remove(bpv);
                Z88.getInstance().getMemory().clearWatchpoint(wpAddress);
            }
        } else {
            Watchpoint bp = new Watchpoint(wpAddress);
            bp.databusWrite = false; // only read watchpoint
            watchPoints.put(bp, bp);
            Z88.getInstance().getMemory().setReadWatchpoint(wpAddress);
        }
    }

    /**
     * Add (if not created) or remove Read/Write watchpoint (if prev. created).
     * A read watchpoint is only removed if both read & write signals are
     * disabled.
     *
     * @param wpAddress 24bit extended address
     * @param stopStatus
     */
    public void toggleReadWatchpoint(int wpAddress, boolean stopStatus) {
        bpSearchKey.setWpAddress(wpAddress);
        Watchpoint bpv = (Watchpoint) watchPoints.get(bpSearchKey);
        if (bpv != null) {
            Z88.getInstance().getMemory().clearReadWatchpoint(wpAddress);
            if (bpv.databusWrite == true) {
                // just disable read-signal, it is enabled for write
                bpv.databusRead = false;
            } else {
                // remove watchpoint, and clear read/write watches in memory
                watchPoints.remove(bpv);
                Z88.getInstance().getMemory().clearWatchpoint(wpAddress);
            }
        } else {
            Watchpoint bp = new Watchpoint(wpAddress, stopStatus);
            bp.databusWrite = false;
            watchPoints.put(bp, bp);
            Z88.getInstance().getMemory().setReadWatchpoint(wpAddress);
        }
    }

    /**
     * Add (if not created) or remove R/W watchpoint (if prev. created).
     *
     * @param wpAddress 24bit extended address
     * @param brkpCmds
     */
    public void toggleReadWatchpoint(int wpAddress, ArrayList<String> brkpCmds) {
        bpSearchKey.setWpAddress(wpAddress);
        Watchpoint bpv = (Watchpoint) watchPoints.get(bpSearchKey);
        if (bpv != null) {
            Z88.getInstance().getMemory().clearReadWatchpoint(wpAddress);
            if (bpv.databusWrite == true) {
                // just disable read-signal, it is enabled for write
                bpv.databusRead = false;
            } else {
                // remove watchpoint, and clear read/write watches in memory
                watchPoints.remove(bpv);
                Z88.getInstance().getMemory().clearWatchpoint(wpAddress);
            }
        } else {
            Watchpoint bp = new Watchpoint(wpAddress, brkpCmds);
            bp.databusWrite = false; // only read watchpoint
            watchPoints.put(bp, bp);
            Z88.getInstance().getMemory().setReadWatchpoint(wpAddress);
        }
    }

    /**
     * Add (if not created) or remove Write watchpoint (if previously created).
     * A read watchpoint is only removed if both read & write signals are
     * disabled.
     *
     * @param wpAddress 24bit extended address
     */
    public void toggleWriteWatchpoint(int wpAddress) {
        bpSearchKey.setWpAddress(wpAddress);
        Watchpoint bpv = (Watchpoint) watchPoints.get(bpSearchKey);
        if (bpv != null) {
            Z88.getInstance().getMemory().clearWriteWatchpoint(wpAddress);
            if (bpv.databusRead == true) {
                // don't delete watchpoint, it is enabled for read
                bpv.databusWrite = false;
            } else {
                // remove watchpoint, and clear read/write watches in memory
                watchPoints.remove(bpv);
                Z88.getInstance().getMemory().clearWatchpoint(wpAddress);
            }
        } else {
            Watchpoint bp = new Watchpoint(wpAddress);
            bp.databusRead = false; // only enable write watchpoint
            watchPoints.put(bp, bp);
            Z88.getInstance().getMemory().setWriteWatchpoint(wpAddress);
        }
    }

    /**
     * Add (if not created) or remove Write watchpoint (if prev. created).
     * A read watchpoint is only removed if both read & write signals are
     * disabled.
     *
     * @param wpAddress 24bit extended address
     * @param stopStatus
     */
    public void toggleWriteWatchpoint(int wpAddress, boolean stopStatus) {
        bpSearchKey.setWpAddress(wpAddress);
        Watchpoint bpv = (Watchpoint) watchPoints.get(bpSearchKey);
        if (bpv != null) {
            Z88.getInstance().getMemory().clearWriteWatchpoint(wpAddress);
            if (bpv.databusRead == true) {
                // just disable write-signal, it is enabled for read
                bpv.databusWrite = false;
            } else {
                // remove watchpoint, and clear read/write watches in memory
                watchPoints.remove(bpv);
                Z88.getInstance().getMemory().clearWatchpoint(wpAddress);
            }
        } else {
            Watchpoint bp = new Watchpoint(wpAddress, stopStatus);
            bp.databusRead = false; // only enable write watchpoint
            watchPoints.put(bp, bp);
            Z88.getInstance().getMemory().setWriteWatchpoint(wpAddress);
        }
    }

    /**
     * Add (if not created) or remove Write Watchpoint (if prev. created).
     *
     * @param wpAddress 24bit extended address
     * @param brkpCmds
     */
    public void toggleWriteWatchpoint(int wpAddress, ArrayList<String> brkpCmds) {
        bpSearchKey.setWpAddress(wpAddress);
        Watchpoint bpv = (Watchpoint) watchPoints.get(bpSearchKey);
        if (bpv != null) {
            Z88.getInstance().getMemory().clearWriteWatchpoint(wpAddress);
            if (bpv.databusRead == true) {
                // just disable write-signal, it is enabled for read
                bpv.databusWrite = false;
            } else {
                // remove watchpoint, and clear read/write watches in memory
                watchPoints.remove(bpv);
                Z88.getInstance().getMemory().clearWatchpoint(wpAddress);
            }
        } else {
            Watchpoint bp = new Watchpoint(wpAddress, brkpCmds);
            bp.databusRead = false; // only enable write watchpoint
            watchPoints.put(bp, bp);
            Z88.getInstance().getMemory().setWriteWatchpoint(wpAddress);
        }
    }

    /**
     * Add (if not created) or remove Read/Write watchpoint (if prev. created).
     *
     * @param wpAddress 24bit extended address
     * @param stopStatus
     */
    public void toggleWatchpoint(int wpAddress, boolean stopStatus) {
        Watchpoint bp = new Watchpoint(wpAddress, stopStatus);
        if (watchPoints.containsKey(bp) == false) {
            watchPoints.put(bp, bp);
            Z88.getInstance().getMemory().setWatchpoint(wpAddress);
        } else {
            watchPoints.remove(bp);
            Z88.getInstance().getMemory().clearWatchpoint(wpAddress);
        }
    }

    /**
     * Add (if not created) or remove R/W watchpoint (if prev. created).
     *
     * @param wpAddress 24bit extended address
     * @param brkpCmds
     */
    public void toggleWatchpoint(int wpAddress, ArrayList<String> brkpCmds) {
        Watchpoint bp = new Watchpoint(wpAddress, brkpCmds);
        if (watchPoints.containsKey(bp) == false) {
            watchPoints.put(bp, bp);
            Z88.getInstance().getMemory().setWatchpoint(wpAddress);
        } else {
            watchPoints.remove(bp);
            Z88.getInstance().getMemory().clearWatchpoint(wpAddress);
        }
    }

    public boolean hasCommands(int wpAddress) {
        bpSearchKey.setWpAddress(wpAddress);
        Watchpoint bpv = (Watchpoint) watchPoints.get(bpSearchKey);
        if (bpv != null) {
            return bpv.hasCommands();
        } else {
            return false;
        }
    }

    public void runCommands(int wpAddress) {
        bpSearchKey.setWpAddress(wpAddress);
        Watchpoint wpv = (Watchpoint) watchPoints.get(bpSearchKey);
        if (wpv != null & wpv.hasCommands()) {
            wpv.runCommands();
        }
    }

    /**
     * Check if this watchpoint has been created.
     *
     * @param wpAddress 24bit extended (watchpoint) address
     * @return true if watchpoint was found, else false.
     */
    public boolean isCreated(int wpAddress) {
        bpSearchKey.setWpAddress(wpAddress);
        Watchpoint wpv = (Watchpoint) watchPoints.get(bpSearchKey);
        if (wpv != null) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Return <true> if watchpoint will stop Z80 execution. (<false> means that
     * it is a display watchpoint)
     *
     * @param wpAddress 24bit extended address
     * @return true, if watchpoint is defined to stop execution.
     */
    public boolean isStoppable(int wpAddress) {
        bpSearchKey.setWpAddress(wpAddress);

        Watchpoint wpv = (Watchpoint) watchPoints.get(bpSearchKey);
        if (wpv != null && wpv.stop == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Mark watchpoint as active.
     *
     * @param wpAddress
     */
    public void activate(int wpAddress) {
        bpSearchKey.setWpAddress(wpAddress);

        Watchpoint bpv = (Watchpoint) watchPoints.get(bpSearchKey);
        if (bpv != null) {
            bpv.active = true;
        }
    }

    /**
     * Mark watchpoint as suspended.
     *
     * @param wpAddress
     */
    public void suspend(int wpAddress) {
        bpSearchKey.setWpAddress(wpAddress);

        Watchpoint bpv = (Watchpoint) watchPoints.get(bpSearchKey);
        if (bpv != null) {
            bpv.active = false;
        }
    }

    /**
     * Return <true> if watchpoint is active (<false> if watchpoint is
     * suspended; ie. will be ignored)
     *
     * @param wpAddress 24bit extended address
     * @return true, if watchpoint is defined as active
     */
    public boolean isActive(int wpAddress) {
        bpSearchKey.setWpAddress(wpAddress);

        Watchpoint bpv = (Watchpoint) watchPoints.get(bpSearchKey);
        if (bpv != null && bpv.active == true) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * List watchpoints into String, so that caller decides to display them.
     */
    public String displayWatchpoints() {
        StringBuffer output = new StringBuffer(1024);
        int itemCounter = 0;

        output.append("Watchpoints:\n");
        if (watchPoints.isEmpty() == true) {
            return new String("No Watchpoints defined.");
        } else {
            Iterator keyIterator = watchPoints.entrySet().iterator();

            while (keyIterator.hasNext()) {
                Map.Entry e = (Map.Entry) keyIterator.next();
                Watchpoint bp = (Watchpoint) e.getKey();

                output.append(Dz.extAddrToHex(bp.getWpAddress(), false));
                output.append(bp.databusRead == true ? "r" : "");
                output.append(bp.databusWrite == true ? "w" : "");
                output.append(bp.stop == false ? "[d]" : "");

                if (itemCounter++ == 7) {
                    output.append("\n");
                    itemCounter = 0;
                } else {
                    output.append("\t");
                }
            }
            output.append("\n");
        }

        return output.toString();
    }
    
    /**
     * 
     */
    public String setWatchpointStatus(boolean status) {
        if (watchPoints.isEmpty() == true) {
            return new String("No Watchpoints defined.");
        } else {
            Iterator keyIterator = watchPoints.entrySet().iterator();

            while (keyIterator.hasNext()) {
                Map.Entry e = (Map.Entry) keyIterator.next();
                Watchpoint wp = (Watchpoint) e.getKey();
                wp.setActiveStatus(status);
            }
        }

        return watchpointList();
    }
    

    /**
     * List watchpoints into String, that can be saved in a property.<br> Each
     * watchpoint is written in hex, separated with a comma. If a break is a
     * display-watchpoint, it is preceeded with a '[d]'. If no watchpoints are
     * defined, an empty string is returned.
     */
    public String watchpointList() {
        StringBuffer output = new StringBuffer(1024);
        if (watchPoints.isEmpty() == true) {
            return "";
        } else {
            Iterator keyIterator = watchPoints.entrySet().iterator();

            while (keyIterator.hasNext()) {
                Map.Entry e = (Map.Entry) keyIterator.next();
                Watchpoint bp = (Watchpoint) e.getKey();

                output.append(bp.databusRead == true ? "r" : "");
                output.append(bp.databusWrite == true ? "w" : "");
                output.append(bp.stop == false ? "[d]" : "");
                output.append(bp.active == false ? "[s]" : "");
                output.append(Dz.extAddrToHex(bp.getWpAddress(), false));
                if (keyIterator.hasNext() == true) {
                    output.append(",");
                }
            }
        }

        return output.toString();
    }

    /**
     * Set the watchpoint flags in Z88 memory for all currently defined (and
     * active) watchpoints.
     */
    public void installWatchpoints() {
        if (watchPoints.isEmpty() == false) {
            Iterator keyIterator = watchPoints.entrySet().iterator();

            while (keyIterator.hasNext()) {
                Map.Entry e = (Map.Entry) keyIterator.next();
                Watchpoint bp = (Watchpoint) e.getKey();

                Z88.getInstance().getMemory().setWatchpoint(bp.getWpAddress());
            }
        }
    }

    /**
     * Clear the watchpoint flags in Z88 memory.
     */
    public void clearWatchpoints() {
        if (watchPoints.isEmpty() == false) {
            Iterator keyIterator = watchPoints.entrySet().iterator();

            while (keyIterator.hasNext()) {
                Map.Entry e = (Map.Entry) keyIterator.next();
                Watchpoint bp = (Watchpoint) e.getKey();

                Z88.getInstance().getMemory().clearWatchpoint(bp.getWpAddress());
            }
        }
    }

    /**
     * Remove all registered watchpoints within this container (using the
     * displayWatchpoints() method afterwards will return a "No Watchpoints
     * defined." string).<p>
     *
     * This method is typically used when a snapshot is being loaded that might
     * contain a different set of watchpoints; the current set therefore needs
     * to be removed before a new is loaded.
     */
    public void removeWatchpoints() {
        clearWatchpoints(); // first remove watchpoints from memory model..
        watchPoints.clear();
    }

    // The watchpoint container.
    private class Watchpoint {

        private int addressKey;         // the 24bit address of the watchpoint
        private boolean stop;           // true = stoppable watchpoint, false = display watchpoint
        private boolean databusRead;    // true = read watchpoint
        private boolean databusWrite;   // true = write watchpoint
        private boolean active;         // true = watchpoint is active, false = watchpoint is suspended
        private ArrayList<String> commands;     // array of commands to be executed at watchpoint.

        /**
         * Create a default (read & write) watchpoint object.
         *
         * @param wpAddress 24bit extended address
         */
        Watchpoint(int wpAddress) {
            stop = true;    // default behaviour is to stop execution at watchpoint
            databusRead = true;    // enable read watchpoint
            databusWrite = true;   // enable write watchpoint
            active = true;  // when a watchpoint is created it is active by default

            // the encoded key for the SortedSet...
            addressKey = wpAddress;
            commands = null;
        }

        /**
         * Create a read & write watchpoint object that has debug mode commands
         *
         * @param wpAddress 24bit extended address
         * @param one or more debug commands to be executed (separated by ;)
         */
        Watchpoint(int wpAddress, ArrayList<String> cmds) {
            this(wpAddress);
            commands = cmds;
        }

        /**
         * Create a read & write watchpoint object that defines a stop or display point
         *
         * @param wpAddress 24bit extended address
         * @param stopAtAddress true, to stop at watchpoint, false to display and continue
         */
        Watchpoint(int wpAddress, boolean stopAtAddress) {
            this(wpAddress);

            // use <false> to display register status, then continue, <true> to stop execution.
            stop = stopAtAddress;
        }

        private void setWpAddress(int wpAddress) {
            addressKey = wpAddress;
        }

        private int getWpAddress() {
            return addressKey;
        }

        private void setActiveStatus(boolean status) {
            active = status;
        }
        
        // override interface with the actual implementation for this object.
        public int hashCode() {
            return addressKey;  // the unique key is a perfect hash code
        }

        private boolean hasCommands() {
            if (commands != null) {
                return true;
            } else {
                return false;
            }
        }

        private void runCommands() {
            if (commands != null) {
                CommandLine cmdLine = CommandLine.getInstance();

                DebugGui.getInstance().disableConsoleInput(); // don't allow command input while parsing file...
                for (int i = 0; i < commands.size(); i++) {
                    cmdLine.parseCommandLine(commands.get(i));
                }
                DebugGui.getInstance().enableConsoleInput(); // ready for commands from the keyboard again...
            }
        }

        // override interface with the actual implementation for this object.
        public boolean equals(Object bp) {
            if (!(bp instanceof Watchpoint)) {
                return false;
            }

            Watchpoint aWatchpoint = (Watchpoint) bp;
            if (addressKey == aWatchpoint.addressKey) {
                return true;
            } else {
                return false;
            }
        }
    }
}
