/*
 * Z88display.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:hello@bits4fun.net">Gunther Strube</A>
 * (C) Gunther Strube (hello@bits4fun.net) 2000-2021
 *
 */
package com.gitlab.z88.ozvm;

import com.imagero.util.ThreadManager;
import com.madgag.gif.fmsware.*;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Font;
import java.awt.Color;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.Transparency;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.concurrent.*;
import java.util.Timer;
import java.util.TimerTask;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileSystemView;

/**
 * The display renderer of the Z88 virtual machine, updating the Z88 screen 10,
 * 25, 50 or 100 frames per second, depending on runtime configuration.
 */
public class Z88display extends JLabel implements MouseListener,FocusListener {

    /**
     * 10 fps (poll for screen changes every 100 milli-seconds)
     */
    public static final int FPS10 = 0;
    /**
     * 25 fps (poll for screen changes every 40 milli-seconds), default
     */
    public static final int FPS25 = 1;
    /**
     * 50 fps (poll for screen changes every 20 milli-seconds)
     */
    public static final int FPS50 = 2;
    /**
     * 100 fps (poll for screen changes every 10 milli-seconds)
     */
    public static final int FPS100 = 3;
    /**
     * Runtime selection of Z88 screen frames per second
     * (10 fps = every 100ms, 25 fps = every 40ms, 50 fps = every 20ms, 100 fps = every 10ms)
     */
    private static final int fps[] = new int[]{10, 25, 50, 100};
    /**
     * Flash cursor duration frame counter (speed of each frame is controlled in fps[])
     * This frequence was observed as 36/60 fps inverse cursor, followed by 24/60 fps blank cursor in video recording.
     * Elapsed time for inverted cursor is 36/60 is 0.6s = 600ms, followed by 400ms of blank cursor.
     */
    private static final int fcd[] = new int[]{600/100, 600/40, 600/20, 600/10};
    /**
     * Enabled pixel
     */
    private static final int PXCOLON = 0xff461B7D;
    /**
     * Grey enabled pixel
     */
    private static final int PXCOLGREY = 0xff90B0A7;
    /**
     * Empty pixel, when screen is switched on
     */
    private static final int PXCOLOFF = 0xffD2E0B9;
    /**
     * Empty pixel, screen is switched off
     */
    private static final int PXCOLSCROFF = 0xffE0E0E0;
    /**
     * Font attribute Hires Mask (HIRES1)
     */
    private static final int attrHrs = 0x20;
    /**
     * Font attribute Reverse Mask (LORES1 & HIRES1)
     */
    private static final int attrRev = 0x10;
    /**
     * Font attribute Flash Mask (LORES1 & HIRES1)
     */
    private static final int attrFls = 0x08;
    /**
     * Font attribute Grey Mask (all)
     */
    private static final int attrGry = 0x04;
    /**
     * Font attribute Underline Mask (LORES1)
     */
    private static final int attrUnd = 0x02;
    /**
     * Null character (6x8 pixel blank)
     */
    private static final int attrNull = attrHrs | attrRev | attrGry;
    /**
     * Lores cursor (6x8 pixel inverse flashing)
     */
    private static final int attrCursor = attrHrs | attrRev | attrFls;

    /**
     * Internal helper class that represent each frame to be saved into the
     * animated Gif screen movie file.
     */
    private class ScreenFrameAction implements Runnable {

        private static final int EncodeFrame = 1;
        private static final int CloseGifFile = 2;

        /**
         * the current GIF frame number
         */
        private int frameNumber;
        /**
         * the action to be taken: encode a frame or close file stream
         */
        private int frameAction;

        /**
         * the delay to be applied before adding the frame to Gif animation
         */
        private int cachedFrameDelay;

        /**
         * the copy of the screen matrix for the Gif frame
         */
        private int[] cachedDisplayMatrix;

        /**
         * Render a BufferedImage from currently cached Display Matrix
         * returns null, if display matrix were not cached
         * @return
         */
        public BufferedImage renderFrame() {
            BufferedImage img = new BufferedImage(screenPixelWidth, screenPixelHeight, BufferedImage.TYPE_4BYTE_ABGR);

            if (cachedDisplayMatrix != null) {
                img.setRGB(0, 0, screenPixelWidth, screenPixelHeight, cachedDisplayMatrix, 0, screenPixelWidth);

                BufferedImage gifImage = configuration.createCompatibleImage(screenPixelWidth*2, screenPixelHeight*2, Transparency.TRANSLUCENT);

                Graphics graphics = gifImage.createGraphics();
                graphics.drawImage(img,0,0,screenPixelWidth*2,screenPixelHeight*2,null);
                graphics.dispose();

                return gifImage;
            } else {
                return null;
            }
        }

        /**
         * Create a new prepared frame with specified delay (ms), for threaded Gif rendering
         */
        public ScreenFrameAction(final int encodeAction, final int frameNo, final int msFrameDelay) {
            frameNumber = frameNo;
            frameAction = encodeAction;
            cachedFrameDelay = msFrameDelay;

            // copy the current display matrix frame, for threaded Gif frame rendering
            cachedDisplayMatrix = new int[screenPixelWidth * screenPixelHeight];
            System.arraycopy(displayMatrix, 0, cachedDisplayMatrix, 0, displayMatrix.length);
        }

        /**
         * This frame action signals end of Gif animation (it has not a cached display matrix)
         */
        public ScreenFrameAction(final int closeAction) {
            frameAction = closeAction;
            cachedFrameDelay = 0;
            cachedDisplayMatrix = null;
        }

        /**
         * execute the action (in background thread)
         */
        public void run() {
            Thread.currentThread().setName("RenderGifAnimation");
            switch(frameAction) {
                case EncodeFrame:
                    BufferedImage gifImage = renderFrame();
                    // apply delay to current last frame stored in gifEncoder instance
                    gifEncoder.setDelay(cachedFrameDelay);
                    // then add latest frame
                    gifEncoder.addFrame(gifImage);

                    if (recordingMovie == false & movieHelper.hasTasks()) {
                        // only display Gif rendering status, when flushing GIF movie
                        OZvm.getInstance().getGui().getMessageMenu().setText("GIF " + frameNumber + "/" + gifFramesTotal);
                    }
                    break;

                case CloseGifFile:
                    gifEncoder.finish();
                    screenFrameQueue.clear();
                    OZvm.getInstance().getGui().getMessageMenu().setText("");
                    OZvm.displayRtmMessage("Screen recording stopped. Saved to '" + movieFilename + "'.");
                    break;
            }
        }
    }

    /**
     * By default the screen pixel factor is 2 (double screen size is rendered)
     */
    private int screenPixelFactor = 2;

    /**
     * The default pixel width of the Cambridge Z88 LCD
     */
    private int screenPixelWidth = 640;

    /**
     * The default pixel height of the Cambridge Z88 LCD
     */
    private int screenPixelHeight = 64;

    /**
     * Default Screen Base File Row Width for Cambridge Z88 640x64 LCD
     * (The Blink renders the LCD from 106 + 2 bytes screen tokens per row,
     * each row per 256 bytes)
     */
    private int sbrRowWidth = 256;

    /**
     * Amount of SBR control characters (font char + attribute) per SbrRow
     */
    private int sbrCtrlCharsPerRow = screenPixelWidth / 6;

    /**
     * Default size of Z88 Screen Base File (in bytes)
     */
    private int sbfSize = sbrRowWidth * screenPixelHeight/8;

    private Timer scrTimer;

    private boolean renderDoubleScreenSize;

    private GraphicsConfiguration configuration;

    /**
     * The internal screen frame renderer
     */
    private RenderPerMs renderPerMs;
    /**
     * is the screen being updated at the moment, or not...
     */
    private boolean renderRunning;
    /**
     * points at the current framerate group, default 50 fps
     */
    private int curRenderSpeedIndex;
    /**
     * separate Thread to manage movie recording of screen activity
     */
    private ThreadManager movieHelper;

    /**
     * current filename of animated Gif file used during screen recording
     */
    private String movieFilename;
    /**
     * The image (based on pixel data array) to be rendered onto Swing Component
     */
    private BufferedImage image;
    /**
     * Screen dump counter
     */
    private int scrdumpCounter;
    /**
     * Animated Gif Movie Counter
     */
    private int movieCounter;

    /**
     * The currently recording screen movie
     */
    private AnimatedGifEncoder gifEncoder;

    /**
     * Total frames accumulated in Gif Animation
     */
    private int gifFramesTotal;

    /**
     * Accumulated time in ms since last displayed frame
     */
    private int frameDelay;

    /**
     * queue of frames to be encoded as animated Gif's
     */
    private ConcurrentLinkedQueue screenFrameQueue;

    /**
     * Cyclic counter that identifies number of frames displayed per second
     */
    private int frameCounter;
    /**
     * Access to Blink hardware (screen, keyboard, timer registers...)
     */
    private Blink blink;
    /**
     * Access to Memory model
     */
    private Memory memory;
    /**
     * identifies whether screen activity is being recorded or not
     */
    private boolean recordingMovie;
    /**
     * Start cursor flash as dark
     */
    private boolean cursorInverse;
    /**
     * Start text flash as dark, ie. text looks normal for 1 sec
     */
    private boolean flashTextEmpty;
    /**
     * The actual low level pixel video data (used to create the AWT Image)
     */
    private int[] displayMatrix;
    /**
     * A copy of the previously rendered pixel matrix frame.
     */
    private int[] cpyDisplayMatrix;
    /**
     * Set to true, if a pixel was changed since the last screen frame rendering
     */
    private boolean screenChanged;
    /**
     * bank offset pointers to the font pixels in OZ
     */
    private int lores0, lores1, hires0, hires1, sbr;
    /**
     * bank references to the font pixels in OZ
     */
    private int bankLores0, bankLores1, bankHires0, bankHires1, bankSbr;
    private Z80Processor z80Proc;

    /**
     * constructor
     */
    public Z88display() {
        super();

        setHorizontalTextPosition(JLabel.CENTER);
        setVerticalTextPosition(JLabel.CENTER);
        setForeground(Color.BLACK);
        setFont(new Font(getFont().getName(), Font.PLAIN, 14));

        blink = null;
        memory = null;
        z80Proc = null;

        renderDoubleScreenSize = true;
        curRenderSpeedIndex = FPS50;
        movieHelper = new ThreadManager(1);
        screenFrameQueue = new ConcurrentLinkedQueue();
        cursorInverse = true;

        displayMatrix = new int[screenPixelWidth * screenPixelHeight];
        cpyDisplayMatrix = new int[screenPixelWidth * screenPixelHeight];
        image = new BufferedImage(screenPixelWidth, screenPixelHeight, BufferedImage.TYPE_4BYTE_ABGR);

        scrTimer = new Timer(true); // run Timer as daemon
        renderRunning = false;

        configuration = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration();

        this.setToolTipText("Click on this window with the mouse to get Z88 keyboard focus.");
        this.setFocusable(true);
        this.addMouseListener(this);
        this.addFocusListener(this);
        setFocusTraversalKeysEnabled(false); // get TAB key events on canvas
    }

    /**
     * Set Z88 LCD Resolution defined by Blink, as follows:
     *      640x64, 640x256, 640x480
     *      800x256, 800x480
     *
     * OZvm will issue a hard-reset after issuing this method, since the
     * OZ V5+ ROM needs to re-allocate space for SBR on larger resolutions
     *
     */
    public void setBlinkLcdResolution() {
        // prefine defaults, if Blink is not yet available
        int pixelWidth = screenPixelWidth, pixelHeight = screenPixelHeight;

        if (blink != null) {
            // get blink LCD resolutions
            pixelWidth = blink.getSCW() * 8;
            pixelHeight = blink.getSCH() * 8;
        }

        // stop frame rendering
        stop();

        /* OZvm supports two horisontal resolutions; 640 and 800 */
        switch(pixelWidth) {
            case 640:
                screenPixelWidth = pixelWidth;
                break;

            case 800:
                screenPixelWidth = pixelWidth;
                break;

            default:
                // illegal specifications ignored, fall-back to default Z88 LCD
                screenPixelWidth = 640;
                screenPixelHeight = 64;
        }

        /* OZvm supports three vertical resolutions; 64, 320 and 480 */
        switch(pixelHeight) {
            case 64:
            case 320:
            case 480:
                screenPixelHeight = pixelHeight;
                break;

            default:
                // illegal specifications ignored, fall-back to default Z88 LCD
                screenPixelWidth = 640;
                screenPixelHeight = 64;
        }

        /* define SBR row width and size, based on selected resolution */
        if (screenPixelWidth == 640) {
            switch(screenPixelHeight) {
                case 64:                                            // 8 rows, SBR = 2048 bytes (8 pages, standard Z88 LCD)
                case 320:                                           // 40 rows, SBR = 10240 bytes (40 pages)
                case 480:                                           // 60 rows, SBR = 15360 bytes (60 pages)
                    sbrRowWidth = 256;                              // 108 control chars (216 bytes), Standard Blink rendering of 256 bytes
                    sbfSize = screenPixelHeight/8 * sbrRowWidth;
                    break;                                          // OZ handles Window status outside Screen Base File...
            }
        }

        if (screenPixelWidth == 800) {
            switch(screenPixelHeight) {
                case 320:                                           // 40 rows, SBR = 10880 bytes (43 pages)
                case 480:                                           // 60 rows, SBR = 16320 bytes (64 pages)
                    sbrRowWidth = 272;                              // (108+28 control chars + 2) * 2 bytes terminator = 272, New blink render!
                    sbfSize = screenPixelHeight/8 * sbrRowWidth;
                    break;                                          // OZ handles Window status outside SBR...
            }
        }

        displayMatrix = new int[screenPixelWidth * screenPixelHeight];
        cpyDisplayMatrix = new int[screenPixelWidth * screenPixelHeight];

        sbrCtrlCharsPerRow = screenPixelWidth / 6;

        // trigger a re-rendering of screen dimension Gui
        setDoubleScreenSize(renderDoubleScreenSize);

        start();
    }

    /**
     * @return the screenPixelWidth
     */
    public int getScreenPixelWidth() {
        return screenPixelWidth;
    }

    /**
     * @return the screenPixelHeight
     */
    public int getScreenPixelHeight() {
        return screenPixelHeight;
    }

    /**
     *
     * @return true if double screen size rendering is enabled, otherwise false
     */
    public boolean isDoubleScreenSize() {
        return renderDoubleScreenSize;
    }

    /**
     * Enable/Disable Double Screen Size rendering Mode
     * @param mode true = double screen size frames are rendered (1280x128)
     */
    public void setDoubleScreenSize(boolean mode) {
        renderDoubleScreenSize = mode;
        if (mode == true) {
            screenPixelFactor = 2;
        }
        else {
            screenPixelFactor = 1;
        }

        this.setPreferredSize(new Dimension(screenPixelWidth*screenPixelFactor, screenPixelHeight*screenPixelFactor));
        image = new BufferedImage(screenPixelWidth, screenPixelHeight, BufferedImage.TYPE_4BYTE_ABGR);
        renderImageToScreen(); // immediately render an image to refresh the Gui screen..
    }

    /**
     * This method is used by Z88 Class to define the Blink chip for the display
     */
    public void connectBlink(Blink bl) {
        blink = bl;
    }

    /**
     * This method is used by Z88 Class to define the Z80 processor for the display
     */
    public void connectProcessor(Z80Processor z80) {
        z80Proc = z80;
    }

    /**
     * This method is used by Z88 Class to define the memory access for the display
     */
    public void connectMemory(Memory mem) {
        memory = mem;               // access to Z88 memory model (4Mb)
    }

    /**
     * Display a message on the centered position of the LCD, transparent
     * Clearing a message is done by writing an empty string.
     */
    public void setScreenMessage(String msg) {
        setForeground(Color.BLACK);
        setFont(new Font(getFont().getName(), Font.PLAIN, 14));
        setText(msg);
    }

    /**
     * Set the update frequency or frames per second (fps) of the Z88 screen.
     * The following values are possible:
     * <pre>
     *  0: 10 fps (poll for screen changes every 100 milli-seconds)
     *  1: 25 fps (poll for screen changes every 40 milli-seconds)
     *  2: 50 fps (poll for screen changes every 20 milli-seconds)
     *  3: 100 fps (poll for screen changes every 10 milli-seconds - same as Z88 LCD)
     * </pre>
     *
     * 25 fps is the default. Use 10 fps when running the emulator on 'slow'
     * PC's (600Mhz or lower). Only use 50 or 100 fps on high end computers (1
     * Ghz or higher). The real Z88 screen uses 100 fps.
     *
     * @param frameRateIndex
     */
    public void setFrameRate(final int frameRateIndex) {
        stop(); // stop the current frames per second renderer

        curRenderSpeedIndex = frameRateIndex % fps.length; // only array range
        start(); // restart renderer with new timings
    }

    /**
     * Get the current Frame Per Second Index. The following value is returned:
     * <pre>
     *  0: 10 fps (poll for screen changes every 100 milli-seconds)
     *  1: 25 fps (poll for screen changes every 40 milli-seconds)
     *  2: 50 fps (poll for screen changes every 20 milli-seconds)
     *  3: 100 fps (poll for screen changes every 10 milli-seconds - same as Z88 LCD)
     * </pre>
     *
     * @return
     */
    public int getCurrentFrameRate() {
        return curRenderSpeedIndex;
    }

    /**
     * Grab current screen frame pixel matrix and write it to a file, double scaled. Default
     * filename is "z88screen" appended with auto-generated sequence number. File
     * format is png. The screen dump is saved to directory location where OZvm
     * were executed.
     */
    public void grabScreenFrameToFile() {
        FileSystemView filesys = FileSystemView.getFileSystemView();

        BufferedImage img = new BufferedImage(screenPixelWidth, screenPixelHeight, BufferedImage.TYPE_4BYTE_ABGR);
        img.setRGB(0, 0, screenPixelWidth, screenPixelHeight, displayMatrix, 0, screenPixelWidth);

        BufferedImage newImage = configuration.createCompatibleImage(screenPixelWidth*2, screenPixelHeight*2, Transparency.TRANSLUCENT);

        Graphics graphics = newImage.createGraphics();
        graphics.drawImage(img,0,0,screenPixelWidth*2,screenPixelHeight*2,null);
        graphics.dispose();

        File file = new File(filesys.getHomeDirectory() + File.separator
                + "z88screen" + scrdumpCounter++ + ".png");
        OZvm.displayRtmMessage("Screen captured to '" + file.getAbsolutePath() + "'.");

        try {
            ImageIO.write(newImage, "PNG", file);
        } catch (IOException e) {
            // hmm...
        }
    }

    /**
     * Enable/disable recording of Z88 screen into animated GIF movie.
     */
    public void toggleMovieRecording() {
        FileSystemView filesys = FileSystemView.getFileSystemView();

        if (recordingMovie == false) {
            if (movieHelper.hasTasks() == false) {
                // enable screen recording
                movieFilename = filesys.getHomeDirectory() + File.separator + "z88movie" + movieCounter++ + ".gif";

                // initialize animated Gif File Encoder and add initial frame
                gifFramesTotal = 0;
                gifEncoder = new AnimatedGifEncoder();
                gifEncoder.start(movieFilename);
                ScreenFrameAction firstLcdFrame = new ScreenFrameAction(ScreenFrameAction.EncodeFrame, ++gifFramesTotal, 0);
                gifEncoder.addFrame(firstLcdFrame.renderFrame());

                recordingMovie = true;
                OZvm.displayRtmMessage("Screen recording to '" + movieFilename + "' activated.");
            } else {
                JOptionPane.showMessageDialog(null, "Cannot start new screen movie capture, current recording finalizing...", "Screen Movie", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            // stop screen recording; append Gif trailer and close GIF file
            // (executed later by background thread)
            OZvm.displayRtmMessage("Stopping Screen recording...");
            recordingMovie = false;
            screenFrameQueue.add(new ScreenFrameAction(ScreenFrameAction.CloseGifFile));
        }
    }

    private void readBlinkScreenRegisters() {
        // Memory base address of 6x8 pixel user defined fonts.
        lores0 = blink.getPb0Address();
        // Memory base address of 6x8 pixel fonts (normal, bold, Tiny)
        lores1 = blink.getPb1Address();
        // Memory base address of PipeDream Map (max 256x64 pixels)
        hires0 = blink.getPb2Address();
        // Memory base address of 8x8 pixel fonts for OZ window
        hires1 = blink.getPb3Address();
        // Memory base address of Screen Base File (2K or bigger)
        sbr = blink.getSbrAddress();
    }

    /**
     * Add the frame execution to the ThreadManager, which will queue the task
     * and execute it when ready (first executing previously registered tasks).
     */
    private void scheduleFrameAction(final ScreenFrameAction frame) {
        movieHelper.addTask(frame);
    }

    /**
     * Get a copy of the current screen frame
     *
     * @return BufferedImage
     */
    public BufferedImage getScreenFrame() {
        BufferedImage img = new BufferedImage(screenPixelWidth, screenPixelHeight, BufferedImage.TYPE_4BYTE_ABGR);
        img.setRGB(0, 0, screenPixelWidth, screenPixelHeight, displayMatrix, 0, screenPixelWidth);

        return img;
    }

    /**
     * Render a "Z88 screen is switched off" image.
     */
    private void renderDisabledScreenFrame() {
        // assume that screen hasn't changed (status might change inside
        // writePixels() method)...
        screenChanged = false;

        for (int x = 0, n = screenPixelHeight * screenPixelWidth; x < n; x++) {
            setPixel(x, PXCOLSCROFF);
        }

        if (screenChanged == true) {
            // pixels changed on the screen. Create an image, based on pixel
            // matrix, and render it via double buffering to the Awt/Swing component
            renderImageToScreen();
        }
    }

    /**
     * Scan the Screen file in OZ, and render a pixel image if a change was
     * identified since the last displayed pixel image.
     */
    private void renderEnabledScreenFrame() {
        if (screenFrameQueue.isEmpty() == false) {
            // schedule item on the screen frame queue to be executed by a background thread...
            scheduleFrameAction((ScreenFrameAction) screenFrameQueue.poll());
        }

        bankLores0 = lores0 >>> 16;
        lores0 &= 0x3FFF; // convert to bank, offset
        bankLores1 = lores1 >>> 16;
        lores1 &= 0x3FFF;
        bankHires0 = hires0 >>> 16;
        hires0 &= 0x3FFF;
        bankHires1 = hires1 >>> 16;
        hires1 &= 0x3FFF;
        bankSbr = sbr >>> 16;
        sbr &= 0x3FFF;

        int scrBaseCoordX = 0, scrBaseCoordY = 0;
        for (int scrRowOffset = 0; scrRowOffset < sbfSize; scrRowOffset += sbrRowWidth) {
            // scan X rows in screen file
            for (int lineCharOffset = 0; lineCharOffset <= (sbrCtrlCharsPerRow*2); lineCharOffset += 2) {
                // scan 2-byte control characters per row in screen file
                int sbrOffset = sbr + scrRowOffset + lineCharOffset;
                int scrChar = memory.getByte(sbrOffset, bankSbr);
                int scrCharAttr = memory.getByte(sbrOffset + 1, bankSbr);

                if ((scrCharAttr & attrHrs) == 0) {
                    // Draw a LORES1 character (6x8 pixel matrix), char offset
                    // into LORES1 is 9 bits...
                    drawLoresChar(scrBaseCoordX, scrBaseCoordY, scrCharAttr, scrChar);
                    scrBaseCoordX += 6;
                } else {
                    if ((scrCharAttr & attrCursor) == attrCursor) {
                        drawLoresCursor(scrBaseCoordX, scrBaseCoordY, scrCharAttr, scrChar);
                        scrBaseCoordX += 6;
                    } else {
                        if ((scrCharAttr & attrNull) != attrNull) {
                            // Draw a HIRES character (PipeDream MAP / OZ window fonts)
                            drawHiresChar(scrBaseCoordX, scrBaseCoordY, scrCharAttr, scrChar);
                            scrBaseCoordX += 8;
                        }
                    }
                }
            }

            // when a complete screen row (8 pixels deep) has been rendered,
            // find out if pixels remain up to the 639'th pixel;
            // these need to get "blanked", before beginning with the next
            // row...
            if (scrBaseCoordX < screenPixelWidth - 1) {
                for (int y = scrBaseCoordY * screenPixelWidth,
                        n = scrBaseCoordY * screenPixelWidth + 8 * screenPixelWidth;
                        y < n; y += screenPixelWidth) {
                    // render x blank pixels until right edge of screen...
                    for (int bit = 0, m = (screenPixelWidth - scrBaseCoordX); bit < m; bit++) {
                        setPixel(y + scrBaseCoordX + bit, PXCOLOFF);
                    }
                }
            }

            // finally, prepare for next pixel row base (downwards)...
            scrBaseCoordY += 8;
            scrBaseCoordX = 0;
        }

        if (screenChanged == false & frameDelay > 990 ) {
            // induce a screen frame refresh every 1s, when nothing changed
            screenChanged = true;
        }

        if (screenChanged == true) {
            // pixels changed on the screen. Create an image, based on pixel
            // matrix, and render it to the Awt/Swing component
            // optionally, grab a frame for Gif movie recording

            if (recordingMovie == true) {
                // add this changed screen frame
                screenFrameQueue.add(new ScreenFrameAction(ScreenFrameAction.EncodeFrame, ++gifFramesTotal, frameDelay));
            }

            frameDelay = 0; // new frame to be displayed, reset acc. time frame counter
            screenChanged = false;

            renderImageToScreen();
        }
    }

    /**
     * Update the JLabel with a new pixel image of the Z88 screen.
     */
    private void renderImageToScreen() {
        image.setRGB(0, 0, screenPixelWidth, screenPixelHeight, displayMatrix, 0,screenPixelWidth);

        /* testing for double size screen rendering */
        if (renderDoubleScreenSize == false) {
            // display a normal 640x64 screen frame
            this.setIcon(new ImageIcon(image));
        } else {
            BufferedImage newImage = configuration.createCompatibleImage(screenPixelWidth*screenPixelFactor, screenPixelHeight*screenPixelFactor, Transparency.TRANSLUCENT);

            Graphics graphics = newImage.createGraphics();
            graphics.drawImage(image,0,0,screenPixelWidth*screenPixelFactor,screenPixelHeight*screenPixelFactor,null);
            graphics.dispose();

            // make the new big screen frame visible in the GUI.
            this.setIcon(new ImageIcon(newImage));
        }
    }

    /**
     * Draw the character at current position, and overlay with flashing cursor.
     *
     * In cursor mode, neither hardware underline nor grey is functional. Only
     * inverse video flashing on pixel data of character.
     *
     * @param scrBaseCoordX pixel column (0-639)
     * @param scrBaseCoordY pixel row coordinate (0-63)
     * @param charAttr the Screen File attribute fo the character (flashing,
     * reverse, tiny, bold)
     * @param scrChar the offset into the LORES character set (top bits in
     * charAttr).
     */
    private void drawLoresCursor(final int scrBaseCoordX,
            final int scrBaseCoordY, final int charAttr, final int scrChar) {
        int bank, bit;

        // safety: if 640 - X coordinate is less than 6 pixels, then abort...
        if (screenPixelWidth - scrBaseCoordX < 6) {
            return;
        }

        int offset = ((charAttr & 1) << 8) | scrChar;
        if (offset >= 0x1c0) {
            offset = lores0 + ((scrChar & 0x3F) << 3); // User defined graphics
            bank = bankLores0;
        } else {
            offset = lores1 + (offset << 3);
            // Base fonts (tiny, bold), default in ROM.0
            bank = bankLores1;
        }

        // render 8 pixel rows of 6 pixel wide scrChar
        for (int y = scrBaseCoordY * screenPixelWidth,
                n = scrBaseCoordY * screenPixelWidth + screenPixelWidth * 8;
                y < n; y += screenPixelWidth) {

            int charBits = memory.getByte(offset++, bank);
            // fetch current pixel row of char
            if (cursorInverse == true) {
                charBits = ~charBits;
            }

            // render 6 pixels wide...
            int pxOffset = 0;
            for (bit = 32; bit > 0; bit >>>= 1) {
                setPixel(y + scrBaseCoordX + pxOffset++,
                        ((charBits & bit) != 0) ? PXCOLON : PXCOLOFF);
            }
        }
    }

    /**
     * Draw a LORES character (6x8 pixel matrix) at pixel position
     * (scrBaseCoordX,scrBaseCoordY). <br>
     *
     * @param scrBaseCoordX pixel column (0-639)
     * @param scrBaseCoordY pixel row coordinate (0-63)
     * @param charAttr the Screen File attribute fo the character (flashing,
     * reverse, tiny, bold, underline, grey)
     * @param scrChar the offset into the LORES character set (top bits in
     * charAttr).
     */
    private void drawLoresChar(final int scrBaseCoordX,
            final int scrBaseCoordY, final int charAttr, final int scrChar) {
        int bank, bit;
        int pxOn, pxColor;

        // safety: if 640 - X coordinate is less than 6 pixels, then abort...
        if (screenPixelWidth - scrBaseCoordX < 6) {
            return;
        }

        if (((charAttr & attrFls) == attrFls) && flashTextEmpty == true) {
            // render 8 pixel rows of 6 empty pixels, if flashing is enabled and
            // is currently "empty"..
            for (int y = scrBaseCoordY * screenPixelWidth,
                    n = scrBaseCoordY * screenPixelWidth + screenPixelWidth * 8;
                    y < n; y += screenPixelWidth) {
                // render 6 pixels wide...
                for (bit = 0; bit < 6; bit++) {
                    setPixel(y + scrBaseCoordX + bit, PXCOLOFF);
                }
            }
            return; // char render completed...
        }

        // Main draw LORES...
        // define pixel colour; clear ON or GREY
        pxOn = ((charAttr & attrGry) == 0) ? PXCOLON : PXCOLGREY;

        int offset = ((charAttr & 1) << 8) | scrChar;
        if (offset >= 0x1c0) {
            offset = lores0 + ((scrChar & 0x3F) << 3); // User defined graphics
            bank = bankLores0;
        } else {
            offset = lores1 + (offset << 3);
            // Base fonts (tiny, bold), default in ROM.0
            bank = bankLores1;
        }

        // render 8 pixel rows of 6 pixel wide scrChar
        int line = 0;
        for (int y = scrBaseCoordY * screenPixelWidth,
                n = scrBaseCoordY * screenPixelWidth + screenPixelWidth * 8;
                y < n; y += screenPixelWidth) {

            int charBits = memory.getByte(offset++, bank);
            // fetch current pixel row of char
            if ((charAttr & attrRev) == attrRev) {
                charBits = ~charBits;
            }

            // render 6 pixels wide...
            if (line++ == 7) {
                // we've reached the 8th pixel line of the char, VDU underline
                // or not?
                if ((charAttr & attrUnd) == attrUnd) {
                    pxColor = pxOn;
                    if ((charAttr & attrRev) == attrRev) {
                        pxColor = PXCOLOFF; // paint "inverse" underline..
                    }
                    for (bit = 0; bit < 6; bit++) {
                        setPixel(y + scrBaseCoordX + bit, pxColor);
                    }
                    break; // we've drawn an underline in stead of the last
                    // pixel row of the lores char...
                }
            }
            int pxOffset = 0;
            for (bit = 32; bit > 0; bit >>>= 1) {
                setPixel(y + scrBaseCoordX + pxOffset++,
                        ((charBits & bit) != 0) ? pxOn : PXCOLOFF);
            }
        }
    }

    /**
     * Draw a HIRES character (8x8 pixel matrix) at pixel position
     * (scrBaseCoordX,scrBaseCoordY).
     *
     * @param scrBaseCoordX pixel column (0-639)
     * @param scrBaseCoordY pixel row coordinate (0-63)
     * @param charAttr the Screen File attribute fo the character (flashing,
     * grey)
     * @param scrChar the offset into the HIRES character set (top bits in
     * charAttr).
     */
    private void drawHiresChar(final int scrBaseCoordX,
            final int scrBaseCoordY, final int charAttr, final int scrChar) {
        int offset, bank;
        int pxOn, bit;

        // safety: if 640 - X coordinate is less than 8 pixels, then abort...
        if (screenPixelWidth - scrBaseCoordX < 8) {
            return;
        }

        if (((charAttr & attrFls) == attrFls) && flashTextEmpty == true) {
            // render 8 pixel rows of 8 empty pixels, if flashing is enabled and
            // is currently "empty"..
            for (int y = scrBaseCoordY * screenPixelWidth,
                    n = scrBaseCoordY * screenPixelWidth + screenPixelWidth * 8;
                    y < n; y += screenPixelWidth) {

                // render 8 pixels wide...
                for (bit = 0; bit < 8; bit++) {
                    setPixel(y + scrBaseCoordX + bit, PXCOLOFF);
                }
            }

            return;
        }

        // Main draw HIRES, define which font set to use...
        offset = ((charAttr & 3) << 8) | scrChar;
        if (offset >= 0x300) {
            offset = hires1 + (scrChar << 3); // "OZ" window font entries
            bank = bankHires1;
        } else {
            offset = hires0 + (offset << 3); // PipeDream Map entries
            bank = bankHires0;
        }

        // define pixel colour; clear ON or GREY
        pxOn = ((charAttr & attrGry) == 0) ? PXCOLON : PXCOLGREY;

        // render 8 pixel rows of 8 pixel wide scrChar
        for (int y = scrBaseCoordY * screenPixelWidth,
                n = scrBaseCoordY * screenPixelWidth + screenPixelWidth * 8;
                y < n; y += screenPixelWidth) {
            int charBits = memory.getByte(offset++, bank);
            // fetch current pixel row of char
            if ((charAttr & attrRev) == attrRev) {
                charBits = ~charBits;
            }

            int pxOffset = 0;
            // render 8 pixels wide...
            for (bit = 128; bit > 0; bit >>>= 1) {
                setPixel(y + scrBaseCoordX + pxOffset++,
                        ((charBits & bit) != 0) ? pxOn : PXCOLOFF);
            }
        }
    }

    /**
     * Draw a pixel into the low level pixel matrix that represent the screen.
     * The offset is a calculation of X and y coordinates that are handled by
     * the caller. This method also handles the intelligence if the current
     * frame has changed compared to the previous frame. The global flag
     * <screenChanged>is set to <true>if the current frame needs to be produced
     * as an image and displayed into the Swing JLabel component that holds the
     * Z88 Display.
     *
     * @param offset The (Y,X) coordinate offset into the pixel matrix.
     * @param pixelColour The 24bit RGB colour for an enabled, grey, clear or
     * screen off clear pixel.
     */
    private void setPixel(int offset, int pixelColour) {
        displayMatrix[offset] = pixelColour;

        if (cpyDisplayMatrix[offset] != displayMatrix[offset]) {
            cpyDisplayMatrix[offset] = displayMatrix[offset];
            screenChanged = true;
        }
    }

    /**
     * Keep flash counters updated according to FPS settings. <br> Ordinary text
     * flashing changes state each second (text appears one sec. then disappears
     * one sec). Cursor flash inverts 6x8 LORES char 70% of 1 second, remaining
     * 30% renders the char as normal.
     */
    private void flashCounter() {
        if (frameCounter++ > fps[curRenderSpeedIndex]) { // 1 second has passed
            frameCounter = 0;
            flashTextEmpty = !flashTextEmpty; // invert current text flashing mode
        }

        if (frameCounter < fcd[curRenderSpeedIndex]) {
            cursorInverse = true; // most of the time, cursor is black
        } else {
            cursorInverse = false; // rest of the time, cursor is invisible
        }
    }

    /**
     * Render Z88 Display each X ms (runtime adjusted) as long as the Z80 engine
     * is running (The Z80 engine automatically stops the Z88 Display renderer,
     * when the Z80 execution engine stops).
     *
     * The RenderPerMs task is scheduled to run each X milliseconds by scrTimer.
     */
    private class RenderPerMs extends TimerTask {
        // time in milliseconds when screen render polling was run
        private long lastrun;

        // screen was previously turned off
        private boolean screenOff;

        public RenderPerMs() {
            screenOff = false;
            lastrun = System.currentTimeMillis();
        }

        public void run() {
            long msNow = System.currentTimeMillis();
            long msTimeLastFrame = msNow - lastrun;

            if ( msTimeLastFrame >= (1000 / fps[curRenderSpeedIndex]) ) {
                // only allow to poll the screen periodically
                // (skip this queued task, when previosu one was run just "now" (needed to "catch up")
                lastrun = msNow;

                readBlinkScreenRegisters();

                if (((blink.getCom() & Blink.BM_COMLCDON) == 0)) {
                    // Screen is off (usually by using the SHIFT keys, or
                    // Blink is in Coma state)
                    if (screenOff == false) {
                        // only need to render "switched off" screen once
                        renderDisabledScreenFrame();
                        setScreenMessage("Press F6 to switch on Z88");
                        screenOff = true;
                    }

                    return;
                }

                if (sbr == 0 | lores1 == 0 | lores0 == 0 | hires0 == 0 | hires1 == 0) {
                    // Don't render frame if one of the Screen Registers hasn't been setup yet...
                    if (screenOff == false) {
                        renderDisabledScreenFrame();
                        screenOff = true;
                    }

                    return;
                }

                // ----------------------------------------------------------------------
                // finally, it's time to render display...
                // screen is ON and Blink registers are all pointing to font areas...
                // ----------------------------------------------------------------------
                if (screenOff == true)
                    setScreenMessage("");
                screenOff = false;

                // update frame delay of actual time passed since last frame was rendered
                frameDelay += msTimeLastFrame;

                // update cursor flash and ordinary flash counters
                if (z80Proc.isZ80running() == true) {
                    flashCounter();
                }

                renderEnabledScreenFrame();
            }
        }
    }

    /**
     * Stop the fps Z88 screen renderer.
     */
    public void stop() {
        if (renderPerMs != null) {
            renderPerMs.cancel();
            renderRunning = false;
        }
    }

    /**
     * Start fps Z88 screen renderer.
     */
    public void start() {
        if (renderRunning == false) {
            frameDelay = 0;
            renderPerMs = new RenderPerMs();
            scrTimer.scheduleAtFixedRate(renderPerMs, 0,
                    1000 / fps[curRenderSpeedIndex]);

            renderRunning = true;
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
     */
    public void mouseClicked(MouseEvent arg0) {
        grabFocus();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
     */
    public void mouseEntered(MouseEvent arg0) {
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
     */
    public void mouseExited(MouseEvent arg0) {
    }

    /*
     * (non-Javadoc)
     *
     * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
     */
    public void mousePressed(MouseEvent arg0) {
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
     */
    public void mouseReleased(MouseEvent arg0) {
    }

    public void focusGained(FocusEvent e) {
        // reset Z88 Keyboard matrix, to avoid spurious keypresses from operating system
        Z88.getInstance().getKeyboard().resetKeyboardMatrix();
    }

    public void focusLost(FocusEvent e) {
        // not used, but implemented abstract methods due to implements class interface
    }
}
