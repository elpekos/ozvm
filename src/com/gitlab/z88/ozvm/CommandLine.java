/*
 * CommandLine.java
 * This file is part of OZvm.
 *
 * OZvm is free software; you can redistribute it and/or modify it under the terms of the
 * GNU General Public License as published by the Free Software Foundation;
 * either version 2, or (at your option) any later version.
 * OZvm is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along with OZvm;
 * see the file COPYING. If not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 *
 * @author <A HREF="mailto:hello@bits4fun.net">Gunther Strube</A>
 * (C) Gunther Strube (hello@bits4fun.net) 2000-2021
 *
 */
package com.gitlab.z88.ozvm;

import com.gitlab.z88.ozvm.datastructures.ApplicationDor;
import com.gitlab.z88.ozvm.datastructures.ApplicationInfo;
import com.gitlab.z88.ozvm.filecard.FileArea;
import com.gitlab.z88.ozvm.filecard.FileAreaExhaustedException;
import com.gitlab.z88.ozvm.filecard.FileAreaNotFoundException;
import com.gitlab.z88.ozvm.filecard.FileEntry;
import java.awt.Color;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.ListIterator;

/**
 * The OZvm debug command line.
 */
public class CommandLine {

    private static final class singletonContainer {
        static final CommandLine singleton = new CommandLine();
    }

    public static CommandLine getInstance() {
        return singletonContainer.singleton;
    }

    private static final String illegalArgumentMessage = "Illegal Argument";
    private boolean logZ80instructions;
    private Blink blink;
    private Z80Processor z80;

    /**
     * The Z88 disassembly engine
     */
    private Dz dz;
    /**
     * The Breakpoint manager
     */
    private Breakpoints breakPointManager;

    /**
     * the debug command line
     */
    private JConsole console;
    /**
     * The Watchpoint manager
     */
    private Watchpoints watchpointManager;

    /**
     * Access the Z88 memory model
     */
    private Memory memory;

    /**
     * accumulated Z80 T-STATE's when single stepping or tracing
     */
    private int tstates;

    // The collected watchpoint command line arguments.
    private class WatchpointCmdArguments {
        public int argc;
        public int addrStart;               // -1 if not specified
        public int addrEnd;                 // -1 if not specified
        public ArrayList<String> wpCmds;    // null if not specified

        public WatchpointCmdArguments() {
            argc = 0;
            addrStart = -1;
            addrEnd = -1;
            wpCmds = null;
        }
    }

    /**
     * Constructor
     */
    private CommandLine() {

        blink = Z88.getInstance().getBlink();
        z80 = Z88.getInstance().getProcessor();
        memory = Z88.getInstance().getMemory();
        console = DebugGui.getInstance().getConsole();

        dz = Dz.getInstance();
        breakPointManager = z80.getBreakpoints();
        watchpointManager = z80.getWatchpoints();
        tstates = 0;

        consoleOutput("Type 'help' + ENTER for available debugging commands");
        consoleOutput("Type 'run' + ENTER to continue executing Z88");
    }

    private void consoleOutput(String msg) {
        console.println(msg);
    }

    private void consoleOutputErr(String msg) {
        console.println(msg, Color.RED);
    }

    private void cmdHelp() {
        consoleOutput("--------------------------------------------------------------------------------------");
        consoleOutput("Command Reference:");
        consoleOutput("--------------------------------------------------------------------------------------");
        consoleOutput("quitvm                     - quit OZvm desktop application");
        consoleOutput("reset                      - soft reset Cambridge Z88 machine");
        consoleOutput("hreset                     - hard reset Cambridge Z88 machine");
        consoleOutput("loadvm [filename]          - Load z88 default 'boot.z88' or specified snapshot filename");
        consoleOutput("savevm [filename]          - Create a snapshot of Z88 to default 'boot.z88' or filename");
        consoleOutput("fcd1-3 [cmd]               - File Card Management - direct memory API without flap INT");
        consoleOutput("       format              - Format file area (all contents erased)");
        consoleOutput("       cardhdr             - Create/update file area header (no format)");
        consoleOutput("       reclaim             - Reclaim deleted file space");
        consoleOutput("       del filename        - Mark file as deleted in file area");
        consoleOutput("       ipf hostfile        - Import file from desktop into file area");
        consoleOutput("       xpf file hostdir    - Export file entry to desktop directory");
        consoleOutput("       ipd hostdir         - Import all files from desktop directory into file area");
        consoleOutput("       xpc hostdir         - Export all file entries to desktop directory");
        consoleOutput("ldc filename extaddr       - Load code from file into bank+offset");
        consoleOutput("dumpslot X [-b][pathname]  - Dump slot X as 'slotX.epr' file, optionally as bank files");
        consoleOutput("app name                   - Display DOR details for specified application name");
        consoleOutput("apps                       - List all available static application DOR's");
        consoleOutput("");
        consoleOutput("wpcl                       - Clear all watchpoints");
        consoleOutput("wwpd [addr] [addr-range]   - Display/toggle Z80 register dump point at Write watchpoint");
        consoleOutput("wrpd [addr] [addr-range]   - Display/toggle Z80 register dump point at Read watchpoint");
        consoleOutput("wpd [addr] [addr-range]    - Display/toggle Z80 register dump point at Read+Write watchpoint");
        consoleOutput("wwp [addr] [addr-range]    - Display/toggle Z80 Write watchpoints (local/ext. address)");
        consoleOutput("wrp [addr] [addr-range]    - Display/toggle Z80 Read watchpoints (local/ext. address)");
        consoleOutput("wp [addr] [addr-range]     - Display/toggle Z80 Read+Write watchpoints (local/ext. address)");
        consoleOutput("wp + | -                     + activate all watchpoints, - suspend all watchpoints");
        consoleOutput("log                        - Enable/disable Z80 instruction execution logging to file");
        consoleOutput("bozd                       - Display/toggle Z80 register dump at OZ System call");
        consoleOutput("boz                        - Enable/disable Z80 Break at OZ System call");
        consoleOutput("bpcl                       - Clear all bp/bpd breakpoints.");
        consoleOutput("bpd [addr]                 - Display/toggle Z80 register dump point (local/ext. address)");
        consoleOutput("bp [addr]                  - Display/toggle Z80 breakpoint (local/ext. address)");
        consoleOutput("bp + | -                     + activate all breakpoints, - suspend all breakpoints");
        consoleOutput("");
        consoleOutput("sbr [w]                    - Display/set Blink Screen Base Register");
        consoleOutput("pb0-3 [w]                  - Display/set Blink PB0,PB1,PB2 & PB3 16bit registers");
        consoleOutput("tim0-4 [b]                 - Display/set Blink CLOCK registers");
        consoleOutput("tmk,tack,tsta [b]          - Display/set Blink TMK,TACK & TSTA registers");
        consoleOutput("kbd [w]                    - Display KBD matrix, set Blink KBD 16bit register");
        consoleOutput("com,int,sta,ack [b]        - Display/set Blink COM,INT,STA,ACK registers");
        consoleOutput("sr0-3 [b]                  - Display/set Blink Segment Register SR0,SR1,SR2,SR3 bindings");
        consoleOutput("sr                         - Display all Blink Segment Register bank bindings");
        consoleOutput("bl                         - Display Blink register contents");
        consoleOutput("rg                         - Display current Z80 registers");
        consoleOutput("");
        consoleOutput("ts [x]                     - Display or reset current accumulated Z80 T-states to x");
        consoleOutput("                             (T-States are recorded by . and z and trace commands)");
        consoleOutput("");
        consoleOutput("BC DE HL    [w]            - Display or set 16bit registers, eg HL $40CC (set register)");
        consoleOutput("BC' DE' HL' [w]              Full expression is available for assigning, eg hl iy-77");
        consoleOutput("IX IY SP PC [w]              (assign IY-77 address to HL. See =, =$ and =@ for details)");
        consoleOutput("EI/DI                      - Enable/Disable INT flip/flop");
        consoleOutput("f/F                        - Display current Z80 flag register");
        consoleOutput("FZ FC FN                   - Display or set Flag Register bit, FZ 1 enables Zero flag");
        consoleOutput("FS FV FH");
        consoleOutput("A B C D E H L I R    [b]   - Display or set 8bit registers, eg B 40 (set register)");
        consoleOutput("A' B' C' D' E' H' L' [b]     full expression is available (see 16bit registers)");
        consoleOutput("IXH IXL IYH IXL      [b]     Index 8bit registers");
        consoleOutput("");
        consoleOutput("wb extaddr {b}             - Write one or more bytes to ext. addr, eg. wb 201ffe 00 40");
        consoleOutput("m [addr]                   - View memory at default PC, or <addr>");
        consoleOutput("                             local address = 2000, ext. addr = 803fc0 (bank+offset)");
        consoleOutput("vsp [addr]                 - View Stack pointer content at default SP, or <addr>");
        consoleOutput("vbc [+/- offset]           - View BC content at default offset 0, or <offset>");
        consoleOutput("vde [+/- offset]           - View DE content at default offset 0, or <offset>");
        consoleOutput("vhl [+/- offset]           - View HL content at default offset 0, or <offset>");
        consoleOutput("vix [+/- offset]           - View IX content at default offset 0, or <offset>");
        consoleOutput("viy [+/- offset]           - View IY content at default offset 0, or <offset>");
        consoleOutput("dz [addr]                  - Disassemble at default PC or <addr>");
        consoleOutput("                             local address = 3f00, ext. addr = 073fc0 (bank+offset)");
        consoleOutput("");
        consoleOutput("= <expr>                   - calculate expression in decimal, using constants");
        consoleOutput("                             (decimal, $ hex, @ binary, Z80 registers) and operators:");
        consoleOutput("                             +, -, *, /, %, &, |, ^, <, >");
        consoleOutput("                             Ex. = ix-77, = hl*bc, = $ff+a, = $fe + (@111*2)");
        consoleOutput("=$ <expr>                  - return expression in hexadecimal notation");
        consoleOutput("=@ <expr>                  - return expression in binary notation");
        consoleOutput("");
        consoleOutput("stop                       - Stop Z80 CPU (also via F5); Enters command/step mode");
        consoleOutput("x | run                    - leave debugging mode and run Z80 CPU");
        consoleOutput("z                          - Trace CALL/RST Z80 subroutine until RET or breakpoint");
        consoleOutput("                             (records T-states)");
        consoleOutput(".                          - Single step next Z80 instruction - records T-states");
        consoleOutput("trace [addr]               - Trace Z80 instructions (records T-states) until breakpoint");
        consoleOutput("cls                        - Clear debug command output window");
    }


    public int getReg8Expression(String expr) {
        if (Z88.getInstance().getProcessorThread() != null) {
            throw new UnsupportedOperationException("Cannot change Z80 register while CPU is executing!");
        } else {
            int arg = IntExpression.evaluate(expr);
            if (arg < -127 | arg > 255) {
                throw new UnsupportedOperationException("Argument out of range");
            } else {
                return arg;
            }
        }
    }

    public int getReg16Expression(String expr) {
        if (Z88.getInstance().getProcessorThread() != null) {
            throw new UnsupportedOperationException("Cannot change Z80 register while CPU is executing!");
        } else {
            int arg = IntExpression.evaluate(expr);
            if (arg < -32767 | arg > 65535) {
                throw new UnsupportedOperationException("Argument out of range");
            } else {
                return arg;
            }
        }
    }

    public void parseCommandLine(String cmdLineText) {
        
        cmdLineText = cmdLineText.replaceAll("[(]", " ( ");
        cmdLineText = cmdLineText.replaceAll("[)]", " ) ");
        cmdLineText = cmdLineText.replaceAll("[;]", " ; ");

        String[] cmdLineTokens = cmdLineText.split(" ");
        int arg;
        SaveRestoreVM srVm;
        String vmFileName;
        ApplicationInfo appInfo;
        WatchpointCmdArguments wpArgs;

        
        if (cmdLineTokens[0].compareToIgnoreCase("help") == 0) {
            cmdHelp();
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("savevm") == 0) {
            srVm = new SaveRestoreVM();
            vmFileName = OZvm.defaultVmFile;

            if (cmdLineTokens.length > 1) {
                vmFileName = cmdLineTokens[1];
                if (vmFileName.toLowerCase().lastIndexOf(".z88") == -1) {
                    vmFileName += ".z88"; // '.z88' extension was missing.
                }
            }

            try {
                if (Z88.getInstance().getProcessorThread() == null) {
                    srVm.storeSnapShot(vmFileName, false);
                    consoleOutput("Snapshot successfully saved to " + vmFileName);
                } else {
                    consoleOutputErr("Snapshot can only be saved when Z88 is not running.");
                }
            } catch (IOException e) {
                consoleOutputErr("Saving snapshot failed.");
            }

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("loadvm") == 0) {
            srVm = new SaveRestoreVM();
            vmFileName = OZvm.defaultVmFile;

            if (cmdLineTokens.length > 1) {
                vmFileName = cmdLineTokens[1];
                if (vmFileName.toLowerCase().lastIndexOf(".z88") == -1) {
                    vmFileName += ".z88"; // '.z88' extension was missing.
                }
            }

            if (Z88.getInstance().getProcessorThread() == null) {
                try {
                    boolean autorun = srVm.loadSnapShot(vmFileName);
                    consoleOutput("Snapshot successfully installed from " + vmFileName);
                    if (autorun == true) {
                        Z88.getInstance().runZ80Cpu();
                        Z88.getInstance().getDisplay().grabFocus(); // default keyboard input focus to the Z88
                    } else {
                        cmdlineFirstSingleStep();
                    }
                } catch (IOException e) {
                    // loading of snapshot failed - define a default Z88 system
                    // as fall back plan.
                    consoleOutputErr("Installation of snapshot failed. Z88 preset to default system.");
                    memory.setDefaultSystem();
                    z80.reset();
                    blink.resetBlink();
                }
            } else {
                consoleOutputErr("Snapshot can only be installed when Z88 is not running.");
            }

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("cls") == 0) {
            DebugGui.getInstance().clearConsole();
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("apps") == 0) {
            appInfo = new ApplicationInfo();
            for (int slot = 0; slot < 4; slot++) {
                ListIterator appList = appInfo.getApplications(slot);
                if (appList != null) {
                    consoleOutput("Slot " + slot + ":");
                    while (appList.hasNext()) {
                        ApplicationDor appDor = (ApplicationDor) appList.next();
                        consoleOutput(appDor.getAppName() + ": DOR = " + Dz.extAddrToHex(appDor.getThisApp(), true) + ", Entry = " + Dz.extAddrToHex(appDor.getEntryPoint(), true));
                    }
                }
            }

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("app") == 0) {
            appInfo = new ApplicationInfo();
            boolean found = false;
            ListIterator appList;
            ApplicationDor appDor;
            int slot;

            if (cmdLineTokens.length == 2) {
                for (slot = 0; slot < 4; slot++) {
                    appList = appInfo.getApplications(slot);
                    if (appList != null) {
                        while (appList.hasNext()) {
                            appDor = (ApplicationDor) appList.next();
                            if (appDor.getAppName().contains(cmdLineTokens[1])) {
                                found = true;
                                consoleOutput("DOR information, " + appDor.getAppName() + " ( []" + appDor.getKeyLetter() + " ) :");
                                consoleOutput("DOR pointer: " + Dz.extAddrToHex(appDor.getThisApp(), true));
                                consoleOutput("Execution Entry: " + Dz.extAddrToHex(appDor.getEntryPoint(), true) + ", bindings: "
                                        + "S0=" + Dz.byteToHex(appDor.getSegment0BankBinding(), true) + ", "
                                        + "S1=" + Dz.byteToHex(appDor.getSegment1BankBinding(), true) + ", "
                                        + "S2=" + Dz.byteToHex(appDor.getSegment2BankBinding(), true) + ", "
                                        + "S3=" + Dz.byteToHex(appDor.getSegment3BankBinding(), true));
                                consoleOutput("Mth: Topics=" + Dz.extAddrToHex(appDor.getTopics(), true) + ", "
                                        + "Commands=" + Dz.extAddrToHex(appDor.getCommands(), true) + ", "
                                        + "Help=" + Dz.extAddrToHex(appDor.getHelp(), true) + ", "
                                        + "Tokens=" + Dz.extAddrToHex(appDor.getTokens(), true));
                            }
                        }
                    }
                }
            } else {
                consoleOutput("Specify Application name (missing). Available apps are:");
                for (slot = 0; slot < 4; slot++) {
                    appList = appInfo.getApplications(slot);
                    if (appList != null) {
                        while (appList.hasNext()) {
                            appDor = (ApplicationDor) appList.next();
                            consoleOutput(appDor.getAppName());
                        }
                    }
                }
            }

            return;
        }



        if (cmdLineTokens[0].compareToIgnoreCase(".") == 0) {
            if (Z88.getInstance().getProcessorThread() != null) {
                consoleOutputErr("Z88 is running - single stepping ignored.");
                return;
            }

            // do a single step (interrupts are not happening)...
            tstates += z80.step();
            DebugGui.getInstance().refreshZ88HardwareInfo();
            console.truncateLastCmd();
            consoleOutput(Z88Info.dzPcStatus(z80.PC()));
            console.presetCommandLine(Dz.getNextStepCommand());

            return;
        }



        if (cmdLineTokens[0].compareToIgnoreCase("z") == 0) {
            if (Z88.getInstance().getProcessorThread() != null) {
                consoleOutputErr("Z88 is running - subroutine execution ignored.");
            } else {
                // do we really have a subroutine at PC?
                if (Dz.getNextStepCommand().compareTo("z") == 0) {
                    int nextInstrAddress = blink.decodeLocalAddress(dz.getNextInstrAddress(z80.PC()));
                    // always do a single step first before running Z80 engine again
                    // (avoid that an IM 1 comes just before the first instruction to be executed)
                    z80.step();

                    if (breakPointManager.isCreated(nextInstrAddress) == true) {
                        // there's already a breakpoint at that location...
                        Z88.getInstance().runZ80Cpu();
                    } else {
                        // set a temporary breakpoint at next instruction and automatically remove it when the engine stops...
                        Z88.getInstance().runZ80Cpu(nextInstrAddress);
                    }
                } else {
                    // no, do a single step command
                    z80.step();        // single stepping (no interrupts running)...
                    DebugGui.getInstance().refreshZ88HardwareInfo();
                }

                console.truncateLastCmd();
            }

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("trace") == 0) {
            int tsAddress = -1;

            if (Z88.getInstance().getProcessorThread() != null) {
                consoleOutputErr("Z88 is running - tracing ignored.");
                return;
            } else {
                if (cmdLineTokens.length >= 2) {
                    tsAddress = Dz.stringAddr2Integer(cmdLineTokens[1]);
                }

                if (tsAddress == -1) {
                    // no temp. breakpoint specified, do trace continuously
                    tstates += z80.trace();
                } else {
                    if (breakPointManager.isCreated(tsAddress) == true) {
                        // there's already a breakpoint at that location...
                        tstates += z80.trace();
                    } else {
                        // set a temporary breakpoint at specified address and automatically remove it when reached...
                        tstates += z80.trace(tsAddress);
                    }
                }

                DebugGui.getInstance().refreshZ88HardwareInfo();
                consoleOutput(Z88Info.dzPcStatus(z80.PC()));
                console.presetCommandLine(Dz.getNextStepCommand());
            }

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("ts") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    int tsarg = IntExpression.evaluate(cmdLineText.substring(3));
                    if (tsarg < 0) {
                        throw new UnsupportedOperationException("T-STATES argument out of range");
                    } else {
                        tstates = tsarg;
                    }
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("T-STATES=" + tstates);

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("run") == 0 || cmdLineTokens[0].compareToIgnoreCase("x") == 0) {
            // always do a single step first before running Z80 engine again
            // (avoid that an IM 1 comes just before the first instruction to be executed)
            if (Z88.getInstance().getProcessorThread() == null) {
                z80.step();
            }
            if (Z88.getInstance().runZ80Cpu() == false) {
                consoleOutputErr("Z88 is already running.");
            } else {
                // input fields are no longer editable
                DebugGui.getInstance().lockZ88MachinePanel(false);

                // make sure that keyboard focus is available for Z88 (screen)
                OZvm.getInstance().getGui().toFront();
                Z88.getInstance().getDisplay().grabFocus();
            }

            return;
        }



        if (cmdLineTokens[0].compareToIgnoreCase("stop") == 0) {
            if (Z88.getInstance().getProcessor().isZ80ThreadRunning() == true) {
                // exit the Z80Processor.run() method and enter single step debug mode
                Z88.getInstance().getProcessor().stopZ80Execution();

                // if thread is sleeping, there is nothing to stop... so force a wake-up, so Z80 can stop
                Z88.getInstance().getBlink().awakeZ80();

                DebugGui.getInstance().activateDebugCommandLine(); // Activate Debug Command Line Window...
                CommandLine.getInstance().cmdlineFirstSingleStep();
                DebugGui.getInstance().lockZ88MachinePanel(true);
            } else {
                consoleOutputErr("Z88 is already stopped.");
            }

            return;
        }



        if (cmdLineTokens[0].compareToIgnoreCase("di") == 0) {
            if (Z88.getInstance().getProcessorThread() != null) {
                consoleOutputErr("Interrupt state cannot be edited while Z88 is running.");
                return;
            } else {
                z80.IFF1(false);
                z80.IFF2(false);
                consoleOutput("Maskable interrupts disabled.");
            }

            return;
        }

 

        if (cmdLineTokens[0].compareToIgnoreCase("ei") == 0) {
            if (Z88.getInstance().getProcessorThread() != null) {
                consoleOutputErr("Interrupt state cannot be edited while Z88 is running.");
                return;
            } else {
                z80.IFF1(true);
                z80.IFF2(true);
                consoleOutput("Maskable interrupts enabled.");
            }

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("log") == 0) {
            if (z80.izZ80Logged() == true) {
                logZ80instructions = false;
                z80.setZ80Logging(logZ80instructions);
                z80.flushZ80LogCache(); // flush the Z80 instruction log cache, if there's anything in it.
                consoleOutput("Z80 Instruction logging disabled.");
            } else {
                logZ80instructions = true;
                z80.setZ80Logging(logZ80instructions);
                consoleOutput("Z80 Instruction logging enabled.");
            }

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("fcd1") == 0 || cmdLineTokens[0].compareToIgnoreCase("fcd2") == 0 || cmdLineTokens[0].compareToIgnoreCase("fcd3") == 0) {
            fcdCommandline(cmdLineTokens);
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("dz") == 0) {
            dzCommandline(cmdLineTokens);
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("vsp") == 0) {
            vStackCommandline(cmdLineTokens);
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("vbc") == 0) {
            vReg16Commandline("BC", z80.BC(), cmdLineTokens);
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("vde") == 0) {
            vReg16Commandline("DE", z80.DE(), cmdLineTokens);
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("vhl") == 0) {
            vReg16Commandline("HL", z80.HL(), cmdLineTokens);
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("vix") == 0) {
            vReg16Commandline("IX", z80.IX(), cmdLineTokens);
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("viy") == 0) {
            vReg16Commandline("IY", z80.IY(), cmdLineTokens);
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("m") == 0) {
            viewMemory(cmdLineTokens);
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("bl") == 0) {
            consoleOutput(Z88Info.blinkRegisterDump());
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("sr") == 0) {
            consoleOutput(Z88Info.bankBindingInfo());
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("rg") == 0) {
            consoleOutput(Z88Info.z80RegisterInfo());
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("boz") == 0) {
            breakPointManager.toggleBreakOzCall();
            consoleOutput("Break at OZ CALL: " + Boolean.toString(breakPointManager.isBreakOzCallActive()));
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("bozd") == 0) {
            breakPointManager.toggleDisplayOzCall();
            consoleOutput("Display OZ CALL: " + Boolean.toString(breakPointManager.isDisplayOzCallActive()));
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("bp") == 0) {
            try {
                bpCommandline(cmdLineTokens);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("bpcl") == 0) {
            breakPointManager.clearBreakpoints();
            breakPointManager.removeBreakPoints();
            consoleOutput("All breakpoints cleared.");            
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("bpd") == 0) {
            try {
                bpdCommandline(cmdLineTokens);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("wp") == 0) {
            // Read/Write Watchpoint
            try {
                wpArgs = wpCommandline(cmdLineTokens);

                switch (wpArgs.argc) {
                    case 1:
                        if (cmdLineTokens[1].compareTo("+") == 0) {
                            consoleOutput(watchpointManager.setWatchpointStatus(true));
                            return;
                        }
                        if (cmdLineTokens[1].compareTo("-") == 0) {
                            consoleOutput(watchpointManager.setWatchpointStatus(false));
                            return;
                        }
                        
                        watchpointManager.toggleWatchpoint(wpArgs.addrStart);
                        break;
                    case 2:                        
                        if (wpArgs.wpCmds == null) {
                            for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++)
                                watchpointManager.toggleWatchpoint(addr);
                        } else {
                            watchpointManager.toggleWatchpoint(wpArgs.addrStart, wpArgs.wpCmds);
                        }
                        break;
                    case 3:
                        for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++) {
                            watchpointManager.toggleWatchpoint(addr, wpArgs.wpCmds);
                        }
                        break;
                }
            } catch (IOException ex) {}

            consoleOutput(watchpointManager.displayWatchpoints());
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("wpd") == 0) {
            // Read/Write Watchpoint Display (dont stop)
            try {
                wpArgs = wpCommandline(cmdLineTokens);

                switch (wpArgs.argc) {
                    case 1:
                        watchpointManager.toggleWatchpoint(wpArgs.addrStart, false);
                        break;
                    case 2:
                        if (wpArgs.wpCmds == null) {
                            // runtime display doesn't execute debug commands...
                            for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++)
                                watchpointManager.toggleWatchpoint(addr, false);
                        }
                        break;
                }
            } catch (IOException ex) {}

            consoleOutput(watchpointManager.displayWatchpoints());
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("wrp") == 0) {
            // Read Watchpoint
            try {
                wpArgs = wpCommandline(cmdLineTokens);

                switch (wpArgs.argc) {
                    case 1:
                        watchpointManager.toggleReadWatchpoint(wpArgs.addrStart);
                        break;
                    case 2:
                        if (wpArgs.wpCmds == null) {
                            for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++)
                                watchpointManager.toggleReadWatchpoint(addr);
                        } else {
                            watchpointManager.toggleReadWatchpoint(wpArgs.addrStart, wpArgs.wpCmds);
                        }
                        break;
                    case 3:
                        for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++) {
                            watchpointManager.toggleReadWatchpoint(addr, wpArgs.wpCmds);
                        }
                        break;
                }
            } catch (IOException ex) {}

            consoleOutput(watchpointManager.displayWatchpoints());
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("wrpd") == 0) {
            // Read Watchpoint Display (dont stop)
            try {
                wpArgs = wpCommandline(cmdLineTokens);

                switch (wpArgs.argc) {
                    case 1:
                        watchpointManager.toggleReadWatchpoint(wpArgs.addrStart, false);
                        break;
                    case 2:
                        if (wpArgs.wpCmds == null) {
                            // runtime display doesn't execute debug commands...
                            for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++)
                                watchpointManager.toggleReadWatchpoint(addr, false);
                        }
                        break;
                }
            } catch (IOException ex) {}

            consoleOutput(watchpointManager.displayWatchpoints());
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("wwp") == 0) {
            // Write Watchpoint
            try {
                wpArgs = wpCommandline(cmdLineTokens);

                switch (wpArgs.argc) {
                    case 1:
                        watchpointManager.toggleWriteWatchpoint(wpArgs.addrStart);
                        break;
                    case 2:
                        if (wpArgs.wpCmds == null) {
                            for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++)
                                watchpointManager.toggleWriteWatchpoint(addr);
                        } else {
                            watchpointManager.toggleWriteWatchpoint(wpArgs.addrStart, wpArgs.wpCmds);
                        }
                        break;
                    case 3:
                        for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++) {
                            watchpointManager.toggleWriteWatchpoint(addr, wpArgs.wpCmds);
                        }
                        break;
                }
            } catch (IOException ex) {}

            consoleOutput(watchpointManager.displayWatchpoints());
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("wwpd") == 0) {
            // Write Watchpoint Display (don't stop)
            try {
                wpArgs = wpCommandline(cmdLineTokens);

                switch (wpArgs.argc) {
                    case 1:
                        watchpointManager.toggleWriteWatchpoint(wpArgs.addrStart, false);
                        break;
                    case 2:
                        if (wpArgs.wpCmds == null) {
                            // runtime display doesn't execute debug commands...
                            for (int addr=wpArgs.addrStart; addr <= wpArgs.addrEnd; addr++)
                                watchpointManager.toggleWriteWatchpoint(addr, false);
                        }
                        break;
                }
            } catch (IOException ex) {}

            consoleOutput(watchpointManager.displayWatchpoints());
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("wpcl") == 0) {
            watchpointManager.clearWatchpoints();
            watchpointManager.removeWatchpoints();
            consoleOutput("All watchpoints cleared.");
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("wb") == 0) {
            try {
                putByte(cmdLineTokens);
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("ldc") == 0) {
            if (cmdLineTokens.length == 3) {
                try {
                    int extAddress = Integer.parseInt(cmdLineTokens[2], 16);
                    int bank = (extAddress >>> 16) & 0xFF;
                    int offset = extAddress & 0x3FFF;
                    Bank b = memory.getBank(bank);

                    memory.loadBankBinary(b, offset, new File(cmdLineTokens[1]));
                    consoleOutput("File image '" + cmdLineTokens[1] + "' loaded at " + cmdLineTokens[2] + ".");
                } catch (IOException e) {
                    consoleOutputErr("Couldn't load file image at ext.address: '" + e.getMessage() + "'");
                }

            } else {
                consoleOutputErr("incorrect arguments");
            }
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("dumpslot") == 0) {
            boolean exportAsBanks = false;
            int slotNumber;
            String dumpFilename;
            String dumpDir;

            if (cmdLineTokens.length >= 2) {
                slotNumber = Integer.parseInt(cmdLineTokens[1]);
                dumpFilename = "slot" + cmdLineTokens[1] + ".epr";
                dumpDir = System.getProperty("user.home");

                if (slotNumber > 0) {
                    if (cmdLineTokens.length == 2) {
                        // "dumpslot X"
                        // dump the specified slot as a complete file
                        // using a default "slotX.epr" filename
                    } else if (cmdLineTokens.length == 3) {
                        // "dumpslot X -b" or "dumpslot X filename"
                        if (cmdLineTokens[2].compareToIgnoreCase("-b") == 0) {
                            dumpFilename = "slot" + cmdLineTokens[1] + "bank";
                            exportAsBanks = true;
                        } else {
                            dumpFilename = cmdLineTokens[2];
                        }
                    } else if (cmdLineTokens.length == 4) {
                        // "dumpslot X -b base-filename"
                        // base filename (with optional path) for filename.bankNo
                        if (cmdLineTokens[2].compareToIgnoreCase("-b") == 0) {
                            exportAsBanks = true;
                        }

                        File fl = new File(cmdLineTokens[3]);
                        if (fl.isDirectory() == true) {
                            dumpDir = fl.getAbsolutePath();
                            dumpFilename = "slot" + cmdLineTokens[1] + "bank";
                        } else {
                            if (fl.getParent() != null) {
                                dumpDir = new File(fl.getParent()).getAbsolutePath();
                            }

                            dumpFilename = fl.getName();
                        }
                    }

                    if (memory.isSlotEmpty(slotNumber) == false) {
                        try {
                            memory.dumpSlot(slotNumber, exportAsBanks, dumpDir, dumpFilename);
                            consoleOutput("Slot was dumped successfully to " + dumpDir);
                        } catch (FileNotFoundException e1) {
                            consoleOutputErr("Couldn't create file(s)!");
                        } catch (IOException e1) {
                            consoleOutputErr("I/O error while dumping slot!");
                        }
                    } else {
                        consoleOutputErr("Slot is empty!");
                    }
                }

            } else {
                consoleOutputErr("Arguments missing!");
            }

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("com") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setCom(arg);
                }
            }
            consoleOutput(Z88Info.blinkComInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("int") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setInt(arg);
                }
            }
            consoleOutput(Z88Info.blinkIntInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("sta") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setSta(arg);
                }
            }
            consoleOutput(Z88Info.blinkStaInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("kbd") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    Z88.getInstance().getKeyboard().setKeyRow(arg >>> 8, arg & 0xFF);
                }
            }
            consoleOutput(Z88.getInstance().getKeyboard().getKbdMatrixSymbolically());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("ack") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setAck(arg);
                }
            }
            consoleOutput(Z88Info.blinkStaInfo());  // ACK affects STA

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("epr") == 0) {
            consoleOutputErr("Not yet implemented");
            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("tsta") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTsta(arg);
                }
            }
            consoleOutput(Z88Info.blinkTstaInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("tack") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTack(arg);
                }
            }
            consoleOutput(Z88Info.blinkTstaInfo()); // TACK affects TSTA

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("tmk") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTmk(arg);
                }
            }
            consoleOutput(Z88Info.blinkTmkInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("pb0") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setPb0(arg);
                }
            }
            consoleOutput(Z88Info.blinkScreenInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("pb1") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setPb1(arg);
                }
            }
            consoleOutput(Z88Info.blinkScreenInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("pb2") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setPb2(arg);
                }
            }
            consoleOutput(Z88Info.blinkScreenInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("pb3") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setPb3(arg);
                }
            }
            consoleOutput(Z88Info.blinkScreenInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("sbr") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 65535) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setSbr(arg);
                }
            }
            consoleOutput(Z88Info.blinkScreenInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("sr0") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setSegmentBank(0, arg);
                }
            }
            consoleOutput(Z88Info.blinkSegmentsInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("sr1") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setSegmentBank(1, arg);
                }
            }
            consoleOutput(Z88Info.blinkSegmentsInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("sr2") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setSegmentBank(2, arg);
                }
            }
            consoleOutput(Z88Info.blinkSegmentsInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("sr3") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setSegmentBank(3, arg);
                }
            }
            consoleOutput(Z88Info.blinkSegmentsInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("tim0") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTim0(arg);
                }
            }
            consoleOutput(Z88Info.blinkTimersInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("tim1") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTim1(arg);
                }
            }
            consoleOutput(Z88Info.blinkTimersInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("tim2") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTim2(arg);
                }
            }
            consoleOutput(Z88Info.blinkTimersInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("tim3") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTim3(arg);
                }
            }
            consoleOutput(Z88Info.blinkTimersInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("tim4") == 0) {
            if (cmdLineTokens.length == 2) {
                arg = StringEval.toInteger(cmdLineTokens[1]);
                if (arg == -1 | arg > 255) {
                    consoleOutputErr(illegalArgumentMessage);
                    return;
                } else {
                    blink.setTim4(arg);
                }
            }
            consoleOutput(Z88Info.blinkTimersInfo());

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("f") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.F(getReg8Expression(cmdLineText.substring(2)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("F=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("f'") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.Fx(getReg8Expression(cmdLineText.substring(3)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            z80.ex_af_af();
            consoleOutput("F'=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");
            z80.ex_af_af();

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("fz") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    consoleOutputErr("Cannot change Zero flag while Z88 is running!");
                    return;
                }
                if (StringEval.toInteger(cmdLineTokens[1]) == 0) {
                    z80.fZ = false;
                } else {
                    z80.fZ = true;
                }
            } else {
                // toggle/invert flag status
                z80.fZ = !z80.fZ;
            }
            consoleOutput("F=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("fc") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    consoleOutputErr("Cannot change Carry flag while Z88 is running!");
                    return;
                }
                if (StringEval.toInteger(cmdLineTokens[1]) == 0) {
                    z80.fC = false;
                } else {
                    z80.fC = true;
                }
            } else {
                // toggle/invert flag status
                z80.fC = !z80.fC;
            }
            consoleOutput("F=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("fs") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    consoleOutputErr("Cannot change Sign flag while Z88 is running!");
                    return;
                }
                if (StringEval.toInteger(cmdLineTokens[1]) == 0) {
                    z80.fS = false;
                } else {
                    z80.fS = true;
                }
            } else {
                // toggle/invert flag status
                z80.fS = !z80.fS;
            }
            consoleOutput("F=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("fh") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    consoleOutputErr("Cannot change Half Carry flag while Z88 is running!");
                    return;
                }
                if (StringEval.toInteger(cmdLineTokens[1]) == 0) {
                    z80.fH = false;
                } else {
                    z80.fH = true;
                }
            } else {
                // toggle/invert flag status
                z80.fH = !z80.fH;
            }
            consoleOutput("F=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("fn") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    consoleOutputErr("Cannot change Add./Sub. flag while Z88 is running!");
                    return;
                }
                if (StringEval.toInteger(cmdLineTokens[1]) == 0) {
                    z80.fN = false;
                } else {
                    z80.fN = true;
                }
            } else {
                // toggle/invert flag status
                z80.fN = !z80.fN;
            }
            consoleOutput("F=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("fv") == 0) {
            if (cmdLineTokens.length == 2) {
                if (Z88.getInstance().getProcessorThread() != null) {
                    consoleOutputErr("Cannot change Parity flag while Z88 is running!");
                    return;
                }
                if (StringEval.toInteger(cmdLineTokens[1]) == 0) {
                    z80.fPV = false;
                } else {
                    z80.fPV = true;
                }
            } else {
                // toggle/invert flag status
                z80.fPV = !z80.fPV;
            }
            consoleOutput("F=" + Z88Info.z80Flags() + " (" + Dz.byteToBin(z80.F(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("a") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.A(getReg8Expression(cmdLineText.substring(2)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("A=" + Dz.byteToHex(z80.A(), true) + " (" + Dz.byteToBin(z80.A(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("a'") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.Ax(getReg8Expression(cmdLineText.substring(3)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("A'=" + Dz.byteToHex(z80.Ax(), true) + " (" + Dz.byteToBin(z80.Ax(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("b") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.B(getReg8Expression(cmdLineText.substring(2)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("B=" + Dz.byteToHex(z80.B(), true) + " (" + Dz.byteToBin(z80.B(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("c") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.C(getReg8Expression(cmdLineText.substring(2)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("C=" + Dz.byteToHex(z80.C(), true) + " (" + Dz.byteToBin(z80.C(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("b'") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.Bx(getReg8Expression(cmdLineText.substring(3)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("B'=" + Dz.byteToHex(z80.Bx(), true) + " (" + Dz.byteToBin(z80.Bx(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("c'") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.Cx(getReg8Expression(cmdLineText.substring(3)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("C'=" + Dz.byteToHex(z80.Cx(), true) + " (" + Dz.byteToBin(z80.Cx(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("bc") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.BC(getReg16Expression(cmdLineText.substring(3)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("BC=" + Dz.addrToHex(z80.BC(), true) + " (" + z80.BC() + "d)");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("bc'") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.BCx(getReg16Expression(cmdLineText.substring(4)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("BC'=" + Dz.addrToHex(z80.BCx(), true) + " (" + z80.BCx() + "d)");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("d") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.D(getReg8Expression(cmdLineText.substring(2)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("D=" + Dz.byteToHex(z80.D(), true) + " (" + Dz.byteToBin(z80.D(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("e") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.E(getReg8Expression(cmdLineText.substring(2)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("E=" + Dz.byteToHex(z80.E(), true) + " (" + Dz.byteToBin(z80.E(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("d'") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.Dx(getReg8Expression(cmdLineText.substring(3)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("D'=" + Dz.byteToHex(z80.Dx(), true) + " (" + Dz.byteToBin(z80.Dx(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("e'") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.Ex(getReg8Expression(cmdLineText.substring(3)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("E'=" + Dz.byteToHex(z80.Ex(), true) + " (" + Dz.byteToBin(z80.Ex(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("de") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.DE(getReg16Expression(cmdLineText.substring(3)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("DE=" + Dz.addrToHex(z80.DE(), true) + " (" + z80.DE() + "d)");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("de'") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.DEx(getReg16Expression(cmdLineText.substring(4)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("DE'=" + Dz.addrToHex(z80.DEx(), true) + " (" + z80.DEx() + "d)");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("h") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.H(getReg8Expression(cmdLineText.substring(2)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("H=" + Dz.byteToHex(z80.H(), true) + " (" + Dz.byteToBin(z80.H(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("l") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.L(getReg8Expression(cmdLineText.substring(2)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("L=" + Dz.byteToHex(z80.L(), true) + " (" + Dz.byteToBin(z80.L(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("h'") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.Hx(getReg8Expression(cmdLineText.substring(3)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("H'=" + Dz.byteToHex(z80.Hx(), true) + " (" + Dz.byteToBin(z80.Hx(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("l'") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.Lx(getReg8Expression(cmdLineText.substring(3)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("L'=" + Dz.byteToHex(z80.Lx(), true) + " (" + Dz.byteToBin(z80.Lx(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("hl") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.HL(getReg16Expression(cmdLineText.substring(3)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("HL=" + Dz.addrToHex(z80.HL(), true) + " (" + z80.HL() + "d)");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("hl'") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.HLx(getReg16Expression(cmdLineText.substring(4)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("HL'=" + Dz.addrToHex(z80.HLx(), true) + " (" + z80.HLx() + "d)");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("ixh") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.IXH(getReg8Expression(cmdLineText.substring(4)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("IXH=" + Dz.byteToHex(z80.IXH(), true) + " (" + Dz.byteToBin(z80.IXH(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("ixl") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.IXL(getReg8Expression(cmdLineText.substring(4)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("IXL=" + Dz.byteToHex(z80.IXL(), true) + " (" + Dz.byteToBin(z80.IXL(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("iyh") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.IYH(getReg8Expression(cmdLineText.substring(4)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("IYH=" + Dz.byteToHex(z80.IYH(), true) + " (" + Dz.byteToBin(z80.IYH(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("iyl") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.IYL(getReg8Expression(cmdLineText.substring(4)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("IYL=" + Dz.byteToHex(z80.IYL(), true) + " (" + Dz.byteToBin(z80.IYL(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("i") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.I(getReg8Expression(cmdLineText.substring(2)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("I=" + Dz.byteToHex(z80.I(), true) + " (" + Dz.byteToBin(z80.I(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("ix") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.IX(getReg16Expression(cmdLineText.substring(3)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("IX=" + Dz.addrToHex(z80.IX(), true) + " (" + z80.IX() + "d)");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("iy") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.IY(getReg16Expression(cmdLineText.substring(3)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("IY=" + Dz.addrToHex(z80.IY(), true) + " (" + z80.IY() + "d)");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("sp") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.SP(getReg16Expression(cmdLineText.substring(3)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("SP=" + Dz.addrToHex(z80.SP(), true) + " (" + z80.SP() + "d)");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("pc") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.PC(getReg16Expression(cmdLineText.substring(3)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput(Z88Info.dzPcStatus(z80.PC()));

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("r") == 0) {
            if (cmdLineTokens.length >= 2) {
                try {
                    z80.R(getReg8Expression(cmdLineText.substring(2)));
                } catch (Exception e) {
                    consoleOutputErr(e.getMessage());
                }
            }
            consoleOutput("R=" + Dz.byteToHex(z80.R(), true) + " (" + Dz.byteToBin(z80.R(), true) + ")");

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("=") == 0) {
            try {
                consoleOutput( Integer.toString(IntExpression.evaluate(cmdLineText.substring(2)), 10));
            } catch (Exception e) {
                consoleOutputErr("Illegal expression");
            };

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("=$") == 0) {
            try {
                consoleOutput( Integer.toString(IntExpression.evaluate(cmdLineText.substring(3)), 16) + "h");
            } catch (Exception e) {
                consoleOutputErr("Illegal expression");
            };

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("=@") == 0) {
            try {
                consoleOutput( Integer.toString(IntExpression.evaluate(cmdLineText.substring(3)), 2) + "b");
            } catch (Exception e) {
                consoleOutputErr("Illegal expression");
            };

            return;
        }


        if (cmdLineTokens[0].compareToIgnoreCase("quitvm") == 0) {
            System.exit(0);
        }

        if (cmdLineTokens[0].compareToIgnoreCase("reset") == 0) {
            Z88.getInstance().pressResetButton();
            return;
        }

        if (cmdLineTokens[0].compareToIgnoreCase("hreset") == 0) {
            Z88.getInstance().hardReset();
            return;
        }
        
        consoleOutputErr("Unknown command. Try 'help'.");
    }

    /**
     * Display current Z80 instruction and simple register dump, and preset a
     * single stepping or subroutine debug command.
     */
    public void cmdlineFirstSingleStep() {
        consoleOutput(Z88Info.dzPcStatus(z80.PC()));
        DebugGui.getInstance().refreshZ88HardwareInfo();
        console.presetCommandLine(Dz.getNextStepCommand());
        console.grabFocus();   // Z88 is stopped, get focus to debug command line.
    }

    private void fcdCommandline(String[] cmdLineTokens) {
        try {
            if (cmdLineTokens.length == 1) {
                // no sub-commands are specified, just list file area contents...
                FileArea fa = new FileArea(cmdLineTokens[0].getBytes()[3] - 48);
                ListIterator fileEntries = fa.getFileEntries();

                if (fileEntries == null) {
                    consoleOutput("File area is empty.");
                } else {
                    consoleOutput("File area:");
                    while (fileEntries.hasNext()) {
                        FileEntry fe = (FileEntry) fileEntries.next();
                        consoleOutput(fe.getFileName()
                                + ((fe.isDeleted() == true) ? " [d]" : "")
                                + ", size=" + fe.getFileLength() + " bytes"
                                + ", entry=" + Dz.extAddrToHex(fe.getFileEntryPtr(), true));
                    }
                }
            } else if (cmdLineTokens.length == 2 & cmdLineTokens[1].compareToIgnoreCase("format") == 0) {
                // create or (re)format file area
                if (FileArea.create(cmdLineTokens[0].getBytes()[3] - 48, true) == true) {
                    consoleOutput("File area were created/formatted.");
                } else {
                    consoleOutputErr("File area could not be created/formatted.");
                }

            } else if (cmdLineTokens.length == 2 & cmdLineTokens[1].compareToIgnoreCase("cardhdr") == 0) {
                // just create a file area header
                if (FileArea.create(cmdLineTokens[0].getBytes()[3] - 48, false) == true) {
                    consoleOutput("File area header were created.");
                } else {
                    consoleOutputErr("File area header could not be created.");
                }

            } else if (cmdLineTokens.length == 2 & cmdLineTokens[1].compareToIgnoreCase("reclaim") == 0) {
                // reclaim deleted file space
                FileArea fa = new FileArea(cmdLineTokens[0].getBytes()[3] - 48);
                fa.reclaimDeletedFileSpace();
                consoleOutput("Deleted files have been removed from file area.");

            } else if (cmdLineTokens.length == 3 & cmdLineTokens[1].compareToIgnoreCase("del") == 0) {
                // mark file as deleted
                FileArea fa = new FileArea(cmdLineTokens[0].getBytes()[3] - 48);
                if (fa.markAsDeleted(cmdLineTokens[2]) == true) {
                    consoleOutput("File was marked as deleted.");
                } else {
                    consoleOutputErr("File not found.");
                }

            } else if (cmdLineTokens.length == 3 & cmdLineTokens[1].compareToIgnoreCase("ipf") == 0) {
                // import file from host file system into file area...
                FileArea fa = new FileArea(cmdLineTokens[0].getBytes()[3] - 48);
                fa.importHostFile(new File(cmdLineTokens[2]));
                consoleOutput("File " + cmdLineTokens[2] + " was successfully imported.");

            } else if (cmdLineTokens.length == 3 & cmdLineTokens[1].compareToIgnoreCase("ipd") == 0) {
                // import all files from host file system directory into file area...
                FileArea fa = new FileArea(cmdLineTokens[0].getBytes()[3] - 48);
                fa.importHostFiles(new File(cmdLineTokens[2]));
                consoleOutput("Directory '" + cmdLineTokens[2] + "' was successfully imported.");

            } else if (cmdLineTokens.length == 3 & cmdLineTokens[1].compareToIgnoreCase("xpc") == 0) {
                // export all files from file area to directory on host file system..
                FileArea fa = new FileArea(cmdLineTokens[0].getBytes()[3] - 48);
                ListIterator fileEntries = fa.getFileEntries();
                if (fa.getActiveFileCount() == 0) {
                    consoleOutputErr("No files available to export.");
                } else {
                    while (fileEntries.hasNext()) {
                        FileEntry fe = (FileEntry) fileEntries.next();

                        if (fe.isDeleted() == false) {
                            // strip the "oz" path of the filename
                            String hostFileName = fe.getFileName();
                            hostFileName = hostFileName.substring(hostFileName.lastIndexOf("/") + 1);
                            // and build a complete file name for the host file system
                            hostFileName = cmdLineTokens[2] + File.separator + hostFileName;

                            // create a new file in specified host directory
                            RandomAccessFile expFile = new RandomAccessFile(hostFileName, "rw");
                            expFile.write(fe.getFileImage()); // export file image to host file system
                            expFile.close();

                            consoleOutput("Exported " + fe.getFileName() + " to " + hostFileName);
                        }
                    }
                }

            } else if (cmdLineTokens.length == 4 & cmdLineTokens[1].compareToIgnoreCase("xpf") == 0) {
                // export file from file area to directory on host file system
                FileArea fa = new FileArea(cmdLineTokens[0].getBytes()[3] - 48);
                FileEntry fe = fa.getFileEntry(cmdLineTokens[2]);
                if (fe == null) {
                    consoleOutputErr("File not found.");
                } else {
                    // strip the "oz" path of the filename
                    String hostFileName = fe.getFileName();
                    hostFileName = hostFileName.substring(hostFileName.lastIndexOf("/") + 1);
                    // and build a complete file name for the host file system
                    hostFileName = cmdLineTokens[3] + File.separator + hostFileName;

                    // create a new file in specified host directory
                    RandomAccessFile expFile = new RandomAccessFile(hostFileName, "rw");
                    expFile.write(fe.getFileImage()); // export file image to host file system
                    expFile.close();

                    consoleOutput("Exported " + fe.getFileName() + " to " + hostFileName);
                }
            } else {
                consoleOutputErr("Unknown file card command or missing arguments.");
            }
        } catch (FileAreaNotFoundException e) {
            consoleOutputErr("No file area found in slot.");
        } catch (FileAreaExhaustedException e) {
            consoleOutputErr("No more room in file area. One or several files could not be imported.");
        } catch (IOException e) {
            consoleOutputErr("I/O error occurred during import/export of files.");
        }
    }

    private WatchpointCmdArguments wpCommandline(String[] cmdLineTokens) throws IOException {
        WatchpointCmdArguments wpArgs = new WatchpointCmdArguments();

        if (cmdLineTokens.length >= 2) {
            // first argument is startaddress (or maybe just a single address)
            wpArgs.argc = 1;
            
            if (cmdLineTokens[1].compareTo("+") == 0 || cmdLineTokens[1].compareTo("-") == 0) {
               wpArgs.addrStart = 0;
               return wpArgs;
            } else {
                wpArgs.addrStart = Dz.stringAddr2Integer(cmdLineTokens[1]);                
            }
        }

        if (cmdLineTokens.length >= 3) {
            // 2nd argument either end address or "+" length specifier or debug commands
            wpArgs.argc++;
            int tokenIdx = 2;

            if (cmdLineTokens[tokenIdx].compareTo("(") == 0) {
                // parse rest of command line for commands to execute at watch point
                wpArgs.wpCmds = parseDebugCmds(cmdLineTokens, tokenIdx);

                return wpArgs;
            }

            if (cmdLineTokens[tokenIdx].startsWith("+") == true) {
               // length specifier
               wpArgs.addrEnd = wpArgs.addrStart + Integer.parseInt(cmdLineTokens[tokenIdx++].substring(1)) - 1;
            } else {
                // 3rd argument is an end-range address
                wpArgs.addrEnd = Dz.stringAddr2Integer(cmdLineTokens[tokenIdx++]);
            }

            if (tokenIdx < cmdLineTokens.length) {
                if ( cmdLineTokens[tokenIdx].compareTo("(") == 0 ) {
                    // parse rest of command line for commands to execute at watch point
                    wpArgs.argc++;
                    wpArgs.wpCmds = parseDebugCmds(cmdLineTokens, tokenIdx);
                }
            }
        }

        return wpArgs;
    }


    private ArrayList<String> parseDebugCmds(String[] cmdLineTokens, int tokenIdx) {
        ArrayList<String> cmds = new ArrayList<String>();
        String cmd = "";

        while (tokenIdx < cmdLineTokens.length & cmdLineTokens[tokenIdx].compareTo("(") != 0) {
            tokenIdx++;
        }

        tokenIdx++; // point at first token of commands
        while (tokenIdx < cmdLineTokens.length) {

            if (cmdLineTokens[tokenIdx].length() > 0) {
                if ((cmdLineTokens[tokenIdx].compareTo(";") == 0 | cmdLineTokens[tokenIdx].compareTo(")") == 0) & cmd.length() > 0) {
                    // command separator or end of commands found, add current command string to list of commands
                    cmd = cmd.trim();
                    cmds.add(cmd);
                    cmd = "";
                } else {
                    cmd += cmdLineTokens[tokenIdx] + " ";
                }
            }

            tokenIdx++;
        }

        return cmds;
    }

    private void bpCommandline(String[] cmdLineTokens) throws IOException {
        int bpAddress;

        if (cmdLineTokens.length >= 2) {
            if (cmdLineTokens[1].compareTo("+") == 0) {
                consoleOutput(breakPointManager.setBreakpointStatus(true));
                return;
            }
            if (cmdLineTokens[1].compareTo("-") == 0) {
                consoleOutput(breakPointManager.setBreakpointStatus(false));
                return;
            }
            
            bpAddress = Dz.stringAddr2Integer(cmdLineTokens[1]);

            if (cmdLineTokens.length == 2) {
                breakPointManager.toggleBreakpoint(bpAddress, true);
            } else {
                // parse rest of command line for commands to execute at break point
                breakPointManager.toggleBreakpoint(bpAddress, parseDebugCmds(cmdLineTokens, 2));
            }
            consoleOutput(breakPointManager.displayBreakpoints());
        }

        if (cmdLineTokens.length == 1) {
            // no arguments, use PC in current bank binding
            consoleOutput(breakPointManager.displayBreakpoints());
        }
    }

    private void bpdCommandline(String[] cmdLineTokens) throws IOException {
        int bpAddress;

        if (cmdLineTokens.length == 2) {
            bpAddress = Dz.stringAddr2Integer(cmdLineTokens[1]);

            breakPointManager.toggleBreakpoint(bpAddress, false);
            consoleOutput(breakPointManager.displayBreakpoints());
        }

        if (cmdLineTokens.length == 1) {
            // no arguments, use PC in current bank binding
            consoleOutput(breakPointManager.displayBreakpoints());
        }
    }

    private void vStackCommandline(String[] cmdLineTokens) {
        int spRegisterAddr = z80.SP(), dzBank = 0;

        if (cmdLineTokens.length == 2) {
            // one argument; an alternate stack pointer address
            spRegisterAddr = Integer.parseInt(cmdLineTokens[1], 16);
        } else {
            if (cmdLineTokens.length != 1) {
                consoleOutputErr("Illegal argument.");
                return;
            }
        }

        for (int vwLine = 14*2; vwLine >= -2*2; vwLine -= 2) {
            String spMem = "SP";
            if (vwLine >= 0)
                spMem +=  "+" + String.format("%02d", vwLine) + " ";
            else
                spMem += String.format("-%02d", Math.abs(vwLine)) + " ";

            spMem += "(" + Dz.extAddrToHex(blink.decodeLocalAddress(spRegisterAddr + vwLine), false) + ") ";
            spMem += Dz.byteToHex(blink.readByte(spRegisterAddr + vwLine+1),false) + Dz.byteToHex(blink.readByte(spRegisterAddr + vwLine),false);
            consoleOutput(spMem);
        }
    }

    private void vReg16Commandline(String regMnem, int registerAddr, String[] cmdLineTokens) {
        int idxOffset = 0, dzBank = 0;

        if (cmdLineTokens.length == 2) {
            // one argument; the offset
            idxOffset = Integer.parseInt(cmdLineTokens[1], 10);
        } else {
            if (cmdLineTokens.length == 1) {
                // no arguments, offset is default 0
                idxOffset = 0;
            } else {
                consoleOutputErr("Illegal argument.");
                return;
            }
        }

        int origAddr = registerAddr + idxOffset;
        for (int vwLine = -8; vwLine <= 8; vwLine++) {
            String regMemStr = "";
            if (idxOffset+vwLine >= 0)
                regMemStr = regMnem + "+" + String.format("%03d", idxOffset+vwLine) + " ";
            else
                regMemStr = regMnem + String.format("-%03d", Math.abs(idxOffset+vwLine)) + " ";

            regMemStr += "(" + Dz.extAddrToHex(blink.decodeLocalAddress(origAddr + vwLine), false) + ") ";
            regMemStr += Dz.byteToHex(blink.readByte(origAddr + vwLine),false);
            consoleOutput(regMemStr);
        }
    }

    private void dzCommandline(String[] cmdLineTokens) {
        boolean localAddressing = true;
        int dzAddr = 0, dzBank = 0;
        StringBuilder dzLine = new StringBuilder(64);

        if (cmdLineTokens.length == 2) {
            // one argument; the local Z80 64K address or a compact 24bit extended address
            dzAddr = Integer.parseInt(cmdLineTokens[1], 16);
            if (dzAddr > 65535) {
                dzBank = (dzAddr >>> 16) & 0xFF;
                dzAddr &= 0xFFFF;   // bank offset (with simulated segment addressing)
                localAddressing = false;
            } else {
                if (cmdLineTokens[1].length() == 6) {
                    // bank defined as '00'
                    dzBank = 0;
                    localAddressing = false;
                } else {
                    localAddressing = true;
                }
            }
        } else {
            if (cmdLineTokens.length == 1) {
                // no arguments, use PC in current bank binding (use local addressing)...
                dzAddr = z80.PC();
                localAddressing = true;
            } else {
                consoleOutputErr("Illegal argument.");
                return;
            }
        }

        if (localAddressing == true) {
            for (int dzLines = 0; dzLines < 16; dzLines++) {
                int origAddr = dzAddr;
                dzAddr = dz.getInstrAscii(dzLine, dzAddr, false, true);
                consoleOutput(Dz.addrToHex(origAddr, false) + " (" + Dz.extAddrToHex(blink.decodeLocalAddress(origAddr), false).toString() + ") " + dzLine.toString());
            }

            console.presetCommandLine("dz " + Dz.addrToHex(dzAddr, false));
        } else {
            // extended addressing
            for (int dzLines = 0; dzLines < 16; dzLines++) {
                int origAddr = dzAddr;
                dzAddr = dz.getInstrAscii(dzLine, dzAddr, dzAddr & 0x3fff, dzBank, false, true);
                consoleOutput(Dz.extAddrToHex((dzBank << 16) | origAddr, false) + " " + dzLine);
            }

            console.presetCommandLine("dz " + Dz.extAddrToHex((dzBank << 16) | dzAddr, false));
        }
    }

    private int getMemoryAscii(StringBuffer memLine, int memAddr) {
        int memHex, memAscii;

        memLine.delete(0, 255);
        for (memHex = memAddr; memHex < memAddr + 16; memHex++) {
            memLine.append(Dz.byteToHex(blink.readByte(memHex), false)).append(" ");
        }

        for (memAscii = memAddr; memAscii < memAddr + 16; memAscii++) {
            int b = blink.readByte(memAscii);
            memLine.append((b >= 32 && b <= 127) ? Character.toString((char) b) : ".");
        }

        return memAscii;
    }

    private int getMemoryAscii(StringBuffer memLine, int memAddr, int memBank) {
        int memHex, memAscii;

        memLine.delete(0, 255);
        for (memHex = memAddr; memHex < memAddr + 16; memHex++) {
            memLine.append(Dz.byteToHex(memory.getByte(memHex, memBank), false)).append(" ");
        }

        for (memAscii = memAddr; memAscii < memAddr + 16; memAscii++) {
            int b = memory.getByte(memAscii, memBank);
            memLine.append((b >= 32 && b <= 127) ? Character.toString((char) b) : ".");
        }

        return memAscii;
    }

    private void putByte(String[] cmdLineTokens) throws IOException {
        int argByte[], memAddress, memBank, aByte;

        if (cmdLineTokens.length >= 3 & cmdLineTokens.length <= 18) {
            memAddress = Integer.parseInt(cmdLineTokens[1], 16);
            memBank = (memAddress >>> 16) & 0xFF;
            memAddress &= 0xFFFF;
            argByte = new int[cmdLineTokens.length - 2];
            for (aByte = 0; aByte < cmdLineTokens.length - 2; aByte++) {
                argByte[aByte] = Integer.parseInt(cmdLineTokens[2 + aByte], 16);
            }
        } else {
            consoleOutputErr("Illegal argument(s).");
            return;
        }

        StringBuffer memLine = new StringBuffer(256);
        getMemoryAscii(memLine, memAddress, memBank);
        consoleOutput("Before:\n" + memLine);
        for (aByte = 0; aByte < cmdLineTokens.length - 2; aByte++) {
            memory.setByte(memAddress + aByte, memBank, argByte[aByte]);
        }

        getMemoryAscii(memLine, memAddress, memBank);
        consoleOutput("After:\n" + memLine);
    }

    private void viewMemory(String[] cmdLineTokens) {
        int memAddr = 0, bankNo = 0, offset = 0;

        if (cmdLineTokens.length == 2) {
            // one argument; the local Z80 64K address or 24bit compact ext. address
            memAddr = Dz.stringAddr2Integer(cmdLineTokens[1]);
        } else {
            if (cmdLineTokens.length == 1) {
                // no arguments, use PC in current bank binding (use local addressing)...
                memAddr = blink.decodeLocalAddress(z80.PC());
            } else {
                consoleOutputErr("Illegal argument.");
                return;
            }
        }

        bankNo = (memAddr >>> 16) & 0xFF;
        offset = memAddr & 0x3FFF;

        Z88.getInstance().getMemory().getBank(bankNo).editMemory(offset, bankNo);
    }
}
